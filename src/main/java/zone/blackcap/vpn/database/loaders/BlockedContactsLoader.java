package zone.blackcap.vpn.database.loaders;

import android.content.Context;
import android.database.Cursor;

import zone.blackcap.vpn.database.DatabaseFactory;
import zone.blackcap.vpn.util.AbstractCursorLoader;

public class BlockedContactsLoader extends AbstractCursorLoader {

  public BlockedContactsLoader(Context context) {
    super(context);
  }

  @Override
  public Cursor getCursor() {
    return DatabaseFactory.getRecipientDatabase(getContext())
                          .getBlocked();
  }

}
