package zone.blackcap.vpn.jobmanager.requirements;

import android.support.annotation.NonNull;

import zone.blackcap.vpn.jobmanager.Job;

public abstract class SimpleRequirement implements Requirement {

  @Override
  public boolean isPresent(@NonNull Job job) {
    return isPresent();
  }

  @Override
  public void onRetry(@NonNull Job job) {
  }

  public abstract boolean isPresent();
}
