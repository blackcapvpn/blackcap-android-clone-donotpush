package zone.blackcap.vpn.contactshare;

public interface Selectable {
  void setSelected(boolean selected);
  boolean isSelected();
}
