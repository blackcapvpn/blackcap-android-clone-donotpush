package zone.blackcap.vpn;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;

import zone.blackcap.vpn.api.BlackcapApi;

public class PurchaseAlertActivity extends AppCompatActivity implements BlackcapApi.ApiResponseListener {

  private View lvStep1, lvStep2, lvStep3, lvStep4, lvStep5;

  private boolean isRemember;
  private String mUsername, mPassword, mCardNumber, mExpdate, mCvv;
  private static ProgressDialog progressDialog;

  EditText etEmail, etCardNumber, etExpdate, etCvv;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_purchase_alert);

    Bundle extras = getIntent().getExtras();
    if (extras != null) {
      Boolean isVPN = extras.getBoolean("isVPN", false);
      if (isVPN) {
        findViewById(R.id.tvAlertMessengerTitle).setVisibility(View.GONE);
        findViewById(R.id.tvAlertMessengerBody).setVisibility(View.GONE);
        findViewById(R.id.tvAlertVpnTitle).setVisibility(View.VISIBLE);
        findViewById(R.id.tvAlertVpnBody).setVisibility(View.VISIBLE);
      }
    }

    lvStep1 = findViewById(R.id.lv_step1);
    lvStep2 = findViewById(R.id.lv_step2);
    lvStep3 = findViewById(R.id.lv_step3);
    lvStep4 = findViewById(R.id.lv_step4);
    lvStep5 = findViewById(R.id.lv_step5);

    etEmail = findViewById(R.id.ev_email);
    etCardNumber = findViewById(R.id.ev_card_number);
    etExpdate = findViewById(R.id.ev_datetime);
    etCvv = findViewById(R.id.ev_cvc);

    SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
    String savedUsername = sharedPreferences.getString("username", null);
    mPassword = sharedPreferences.getString("password", null);
    etEmail.setText(savedUsername);

    etExpdate.addTextChangedListener(new TextWatcher() {
      boolean _ignore = false;

      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        if (_ignore) return;

        _ignore = true;
        if (etExpdate.getText().length() == 3 && etExpdate.getText().toString().contains("/")) {
          etExpdate.setText(etExpdate.getText().toString().substring(0,2));
          etExpdate.setSelection(etExpdate.getText().length());
        } else if (etExpdate.getText().length() == 3 && !etExpdate.getText().toString().contains("/")) {
          etExpdate.setText(etExpdate.getText().toString().substring(0,2) + "/" + etExpdate.getText().toString().substring(2));
          etExpdate.setSelection(etExpdate.getText().length());
        }
        _ignore = false;
      }
    });

    findViewById(R.id.btn_step1_no_thanks).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });

    findViewById(R.id.btn_step1_upgrade).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        lvStep1.setVisibility(View.GONE);
        lvStep2.setVisibility(View.VISIBLE);
      }
    });

    findViewById(R.id.btn_step2_cancel).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });

    findViewById(R.id.btn_step2_purchase).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        lvStep2.setVisibility(View.GONE);
        lvStep3.setVisibility(View.VISIBLE);
      }
    });

    findViewById(R.id.btn_step3_cancel).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });

    findViewById(R.id.btn_step3_purchase).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        purchase();
      }
    });

    findViewById(R.id.btn_step4_continue).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });

    findViewById(R.id.btn_step5_cancel).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onBackPressed();
      }
    });

    findViewById(R.id.btn_step5_try_again).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        etCardNumber.setText("");
        etExpdate.setText("");
        etCvv.setText("");

        lvStep5.setVisibility(View.GONE);
        lvStep2.setVisibility(View.VISIBLE);
      }
    });

    findViewById(R.id.btn_remember_me).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        isRemember = !isRemember;
        if (isRemember) {
          ((ImageView)findViewById(R.id.iv_remember_me)).setImageDrawable(getResources().getDrawable(R.drawable.checked));
        } else {
          ((ImageView)findViewById(R.id.iv_remember_me)).setImageDrawable(getResources().getDrawable(R.drawable.unchecked));
        }
      }
    });
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (progressDialog != null && progressDialog.isShowing()) {
      progressDialog.dismiss();
    }
  }

  @Override
  public void onTaskCompleted(BlackcapApi.ApiResponse apiResponse) {
    Crashlytics.setString("apiResponse.requestCode", apiResponse.requestCode.toString());
    Crashlytics.setString("apiResponse.code", apiResponse.code.toString());

    if (apiResponse.requestCode == BlackcapApi.RequestCode.PURCHASE) {
      if (progressDialog != null && progressDialog.isShowing()) {
        progressDialog.dismiss();
      }

      if (apiResponse.code == BlackcapApi.ResponseCode.SUCCESS) {
        SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("card_number", mCardNumber);
        editor.putString("card_expdate", mExpdate);
        editor.putString("card_cvv", mCvv);

        editor.putBoolean("messenger", true);
        editor.putBoolean("vpn", true);
        editor.apply();

        lvStep3.setVisibility(View.GONE);
        lvStep4.setVisibility(View.VISIBLE);

      } else {
        if (apiResponse.err != null) {
          new AlertDialog.Builder(this, R.style.blackcap_Dialog)
              .setTitle(getString(R.string.bc_error))
              .setMessage(apiResponse.err)
              .setPositiveButton(android.R.string.ok, null)
              .show();
        }
        lvStep3.setVisibility(View.GONE);
        lvStep5.setVisibility(View.VISIBLE);
      }
    }
  }

  public void purchase() {

    if (etEmail != null && etEmail.getText().toString().isEmpty()) {
      etEmail.requestFocus();
      return;
    } else if (etCardNumber != null && etCardNumber.getText().toString().isEmpty()) {
      etCardNumber.requestFocus();
      return;
    } else if (etExpdate != null && etExpdate.getText().toString().isEmpty()) {
      etExpdate.requestFocus();
      return;
    } else if (etCvv != null && etCvv.getText().toString().isEmpty()) {
      etCvv.requestFocus();
      return;
    }

    if (etEmail != null && etCardNumber != null && etExpdate != null && etCvv != null) {
      ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
      NetworkInfo networkInfo = null;
      if (connMgr != null) {
        networkInfo = connMgr.getActiveNetworkInfo();
      }

      if (networkInfo != null && networkInfo.isConnected()) {
        SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
        String savedUsername = sharedPreferences.getString("username", null);
        String savedPassword = sharedPreferences.getString("password", null);

        mUsername = etEmail.getText().toString();
        mCardNumber = etCardNumber.getText().toString();
        mExpdate = etExpdate.getText().toString();
        mCvv = etCvv.getText().toString();

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.bc_please_wait));
        progressDialog.setMessage("Purchasing...");
        progressDialog.show();

        new BlackcapApi().purchase(this, mUsername, mPassword, mCardNumber, mExpdate, mCvv);
      } else {
        new AlertDialog.Builder(this, R.style.blackcap_Dialog)
            .setTitle(getString(R.string.bc_error))
            .setMessage(getString(R.string.bc_internet_required))
            .setPositiveButton(android.R.string.ok, null)
            .show();
//        lvStep3.setVisibility(View.GONE);
//        lvStep5.setVisibility(View.VISIBLE);
      }
    }
  }
}
