package zone.blackcap.vpn.recipients;


public interface RecipientModifiedListener {
  public void onModified(Recipient recipient);
}
