package zone.blackcap.vpn;

public class TextSecureExpiredException extends Exception {
  public TextSecureExpiredException(String message) {
    super(message);
  }
}
