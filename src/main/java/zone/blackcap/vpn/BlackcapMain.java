package zone.blackcap.vpn;

import android.annotation.SuppressLint;
import android.app.Activity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import zone.blackcap.vpn.activities.ConfigConverter;
import zone.blackcap.vpn.activities.DisconnectVPN;
import zone.blackcap.vpn.activities.SelectLanguageActivity;
import zone.blackcap.vpn.api.BlackcapApi;
import zone.blackcap.vpn.fragments.NavigationDrawerVPNFragment;
import zone.blackcap.vpn.core.ProfileManager;
import zone.blackcap.vpn.core.VpnStatus;

public class BlackcapMain extends AppCompatActivity implements
        NavigationDrawerVPNFragment.NavigationDrawerCallbacks,
        VpnStatus.StateListener,
        BlackcapApi.ApiResponseListener {

  private static final String DEBUG_TAG = "Blackcap";
  private static final String ERROR_GET = "Unable to retrieve web page. URL may be invalid.";

  private static final int IMPORT_PROFILE = 231;

  private enum ConnectionStatus {Disconnected, Connected, Disconnecting, Connecting}

  private static ConnectionStatus mConnectionStatus = ConnectionStatus.Disconnected;

  private static ArrayList<Server> mServerList;

  private static String mUsername;
  private static String mPassword;

  private static ArrayList<PingTask> mPingTasks = null;

  public static boolean mChangeServer = false;

  private static ProgressDialog mConnectDialog;

  private static String mCountry;
  private static String mIpAddress;

  private static boolean mConnectedOnce = false;

  private FragmentManager mFragmentManager;

  private BasicFragment mBasicFragment;
  private AdvancedFragment mAdvancedFragment;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.blackcap_main);

    mFragmentManager = getFragmentManager();

    NavigationDrawerVPNFragment navigationDrawerVPNFragment = (NavigationDrawerVPNFragment)
        mFragmentManager.findFragmentById(R.id.navigation_drawer);

    navigationDrawerVPNFragment.setUp(
        R.id.navigation_drawer,
        findViewById(R.id.drawer_layout));

    ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);

    mBasicFragment = new BasicFragment();
    mAdvancedFragment = new AdvancedFragment();

    FragmentTransaction transaction = mFragmentManager.beginTransaction();
    transaction.replace(R.id.container, mBasicFragment, "BASIC");
    if (mFragmentManager.getBackStackEntryCount() != 0) {
      transaction.addToBackStack("BASIC");
    }
    transaction.commit();

    if (VpnStatus.isVPNActive()) {
      mConnectionStatus = ConnectionStatus.Connected;

      if (mBasicFragment.getView() != null) {
        ((TextView) mBasicFragment.getView().findViewById(R.id.textView_location)).setText(mCountry);
        ((TextView) mBasicFragment.getView().findViewById(R.id.textView_ipAddress)).setText(mIpAddress);
      }

      if (mAdvancedFragment.getView() != null) {
        ((TextView) mAdvancedFragment.getView().findViewById(R.id.textView_location)).setText(mCountry);
        ((TextView) mAdvancedFragment.getView().findViewById(R.id.textView_ipAddress)).setText(mIpAddress);
      }
    }

    Bundle bundle = getIntent().getExtras();
    if (bundle != null) {
      if (bundle.containsKey("username")) {
        mUsername = bundle.getString("username");
      }

      if (bundle.containsKey("password")) {
        mPassword = bundle.getString("password");
      }
    }

    VpnStatus.addStateListener(this);

    getServerList();
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putString("username", mUsername);
    outState.putString("password", mPassword);
  }

  @Override
  public void onBackPressed() {
    for (int entry = 0; entry < mFragmentManager.getBackStackEntryCount(); entry++) {
      Log.i("Test", "Found fragment: " + mFragmentManager.getBackStackEntryAt(entry).getName());
    }

    if (mFragmentManager.getBackStackEntryCount() > 0) {
      mFragmentManager.popBackStack();
    } else {
      super.onBackPressed();
    }
  }

  public void openNavigationDrawer(View view) {
    ((DrawerLayout) findViewById(R.id.drawer_layout)).openDrawer(findViewById(R.id.navigation_drawer));
  }

  /**
   * Handle back button pressed.
   * @param view
   */
  public void back(View view) {
    onBackPressed();
  }

  /**
   * Switch to basic VPN fragment.
   * @param view
   */
  public void openBasic(View view) {
    mFragmentManager.beginTransaction()
        .replace(R.id.container, mBasicFragment, "BASIC")
        .addToBackStack("BASIC")
        .commit();
  }

  /**
   * Switch to advanced VPN fragment.
   * @param view
   */
  public void openAdvanced(View view) {
    mFragmentManager.beginTransaction()
        .replace(R.id.container, mAdvancedFragment, "ADVANCED")
        .addToBackStack("ADVANCED")
        .commit();
  }

  /**
   * Switch to settings activity.
   */
  public void openSettings() {
    Intent intent = new Intent(this, BlackcapSettings.class);
    startActivity(intent);
  }

  /**
   * Switch to Language activity.
   */
  public void openLanguageActivity() {
    Intent intent = new Intent(this, SelectLanguageActivity.class);
    startActivity(intent);
  }

  /**
   * Switch to licence notice activity.
   */
  public void openLegalNotice() {
    Intent intent = new Intent(this, BlackcapLicence.class);
    startActivity(intent);
  }

  /**
   * Logout.
   */
  public void logout() {
    SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
    SharedPreferences.Editor editor = sharedPreferences.edit();

//    editor.clear().apply();
    editor.putString("username", null);
    editor.putString("password", null);
    editor.putBoolean("save_password", false);
    editor.putBoolean("dont_show_popup", false);
    editor.putBoolean("auto_login", false);
    editor.putBoolean("disconnect_dialog", false);
    editor.apply();

    Intent intent = new Intent(this, BlackcapLogin.class);
    startActivity(intent);

    Intent disconnectVPN = new Intent(this, DisconnectVPN.class);
    startActivity(disconnectVPN);

    finish();
  }

  /**
   * Handle draw item selected.
   * @param position
   */
  @Override
  public void onNavigationDrawerItemSelected(int position) {
    if (position == 0) {
      FragmentTransaction transaction = mFragmentManager.beginTransaction();
      transaction.replace(R.id.container, mBasicFragment, "BASIC");
      if (mFragmentManager.getBackStackEntryCount() != 0) {
        transaction.addToBackStack("BASIC");
      }
      transaction.commit();
    } else if (position == 1) {
      mFragmentManager.beginTransaction()
          .replace(R.id.container, mAdvancedFragment, "ADVANCED")
          .addToBackStack("ADVANCED")
          .commit();
    } else if (position == 2) {
      ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(findViewById(R.id.navigation_drawer));

      Intent intent = new Intent(this, ConversationListActivity.class);
      Bundle bundle = new Bundle();
      intent.putExtras(bundle);
      startActivity(intent);
    } else if (position == 3) {
      String url = "https://blackcap.zone/support.php";

      Intent intent = new Intent(Intent.ACTION_VIEW);
      intent.setData(Uri.parse(url));

      startActivity(intent);

    } else if (position == 4) {
      openLegalNotice();
    } else if (position == 5) {
      openSettings();
    } else if (position == 6) {
      openLanguageActivity();
    } else if (position == 7) {
      logout();
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    Log.d(DEBUG_TAG, this.getClass().getSimpleName());
    if (resultCode != Activity.RESULT_OK || data == null) {
      return;
    }

    if (mConnectDialog != null && !isFinishing()) {
      mConnectDialog.dismiss();
      mConnectDialog = null;
    }

    mConnectDialog = new ProgressDialog(this);
    mConnectDialog.setTitle(R.string.bc_please_wait);
    mConnectDialog.setMessage(getResources().getString(R.string.bc_connecting_dotdotdot));
    mConnectDialog.setCancelable(false);
    mConnectDialog.setCanceledOnTouchOutside(false);
    mConnectDialog.show();

    //After VPN profile is imported, start VPN connection.
    if (requestCode == IMPORT_PROFILE) {
      String profileUUID = data.getStringExtra(VpnProfile.EXTRA_PROFILEUUID);
      VpnProfile mAdapter = ProfileManager.get(this, profileUUID);
      mAdapter.mUsername = mUsername;
      mAdapter.mTransientPW = mPassword;
      ProfileManager.getInstance(this).saveProfile(this, mAdapter);

      Intent intent = new Intent(this, LaunchVPN.class);
      intent.putExtra(LaunchVPN.EXTRA_KEY, mAdapter.getUUID().toString());
      intent.setAction(Intent.ACTION_MAIN);
      startActivity(intent);
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (mConnectDialog != null && mConnectDialog.isShowing()) {
      mConnectDialog.dismiss();
    }
  }

  /**
   * Retrive list of VPN servers on the Black Cap Network
   */
  private void getServerList() {
    Log.d(DEBUG_TAG, this.getClass().getSimpleName());
    ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = connMgr != null ? connMgr.getActiveNetworkInfo() : null;
    if (networkInfo != null && networkInfo.isConnected()) {
      if (mConnectDialog != null && !isFinishing()) {
        mConnectDialog.dismiss();
        mConnectDialog = null;
      }

      mConnectDialog = new ProgressDialog(this);
      mConnectDialog.setTitle(R.string.bc_please_wait);
      mConnectDialog.setMessage(getResources().getString(R.string.bc_getting_servers));
      mConnectDialog.setCancelable(false);
      mConnectDialog.setCanceledOnTouchOutside(false);
      mConnectDialog.show();

      new BlackcapApi().getServerList(this);
//      String query = "https://blackcap.zone/api/status-list.php";
//      new BlackcapMain.DownloadWebpageTask().execute(query);
    } else {
      new AlertDialog.Builder(this, R.style.blackcap_Dialog)
          .setTitle(getString(R.string.bc_error))
          .setMessage(getString(R.string.bc_internet_required))
          .setPositiveButton(android.R.string.ok, null)
          .show();
    }
  }

  @Override
  public void onTaskCompleted(BlackcapApi.ApiResponse apiResponse) {
    Crashlytics.setString("apiResponse.requestCode", apiResponse.requestCode.toString());
    Crashlytics.setString("apiResponse.code", apiResponse.code.toString());

    if (mConnectDialog != null && mConnectDialog.isShowing()) {
      mConnectDialog.dismiss();
      mConnectDialog = null;
    }

    if (apiResponse.requestCode == BlackcapApi.RequestCode.SERVER_LIST) {
      if (apiResponse.code == BlackcapApi.ResponseCode.SUCCESS) {
        if (mServerList == null) {
          mServerList = new ArrayList<>();
        } else {
          mServerList.clear();
        }

        for (Server server: apiResponse.servers) {
          if (server.up) {
            mServerList.add(server);
          }
        }

        if (mServerList.size() > 0) {
          if (mPingTasks != null && !mPingTasks.isEmpty()) {
            mPingTasks.clear();
          }

          for (Server s : mServerList) {
//              new PingTask().execute(s.ip);
          }
        }

        runOnUiThread(() -> {
          List<String> stringList = new ArrayList<>();
          for (Server s : BlackcapMain.mServerList) {
            stringList.add(s.name);
          }

          ArrayAdapter<String> adapter = new ArrayAdapter<>(BlackcapMain.this, android.R.layout.simple_spinner_dropdown_item, stringList);

          Spinner spinner_server = findViewById(R.id.spinner_server);
          if (spinner_server != null) {
            spinner_server.setAdapter(adapter);
          }
        });

        SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
        if (sharedPreferences.getBoolean("auto_connect", false) && !mConnectedOnce) {
          mConnectedOnce = true;
          connect(null);
        }
      } else {
        new AlertDialog.Builder(BlackcapMain.this, R.style.blackcap_Dialog)
                .setTitle(getString(R.string.bc_error))
                .setMessage("Unable to download server list.")
                .setPositiveButton(android.R.string.ok, null)
                .show();
      }
    }
  }

  /**
   * Testing purchase alert here
   * @param view
   */
  public void onDownloadClicked(View view) {
//    Intent alert = new Intent(this, PurchaseAlertActivity.class);
//    startActivity(alert);
  }


  /**
   * Start a VPN connection.
   * @param view
   */
  public void connect(View view) {
    Log.d(DEBUG_TAG, this.getClass().getSimpleName());
    SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
    boolean vpnStatus = sharedPreferences.getBoolean("vpn", false);

    if (!vpnStatus) {
      Intent alert = new Intent(this, PurchaseAlertActivity.class);
      alert.putExtra("isVPN", true);
      startActivity(alert);
      return;
    }

    //Handle "Change" button press
    if (VpnStatus.isVPNActive() && view != null && view.getId() != R.id.button_advanced_change) {
      Intent disconnectVPN = new Intent(this, DisconnectVPN.class);
      startActivity(disconnectVPN);

      return;
    }

    //Check network connection
    ConnectivityManager connMgr = (ConnectivityManager)
        getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = connMgr != null ? connMgr.getActiveNetworkInfo() : null;
    if (networkInfo != null && networkInfo.isConnected() && mServerList != null && mServerList.size() != 0) {
      String server = "";

      //Get requested server if in advanced mode
      if (mAdvancedFragment.getView() != null) {
        Spinner spinner_server = mAdvancedFragment
            .getView()
            .findViewById(R.id.spinner_server);

        for (Server s : mServerList) {
          if (s.name.equals(spinner_server.getSelectedItem())) {
            server = s.domainName;
            mCountry = s.county;
            mIpAddress = s.ip;
            break;
          }
        }
      }

      //Calculate server ranking values
      for (Server s : mServerList) {
        float weight = (float) s.weight / 2.0f;
        float load = (100.0f - (float) s.loadPercentage) / 4.0f;
        float ping = (100.0f - ((float) s.ping / 10.0f)) / 4.0f;
        s.value = weight + load + ping;
      }

      //Rank servers and selected best if not in advanced mode
      if (server.isEmpty()) {
        Collections.sort(mServerList, (lhs, rhs) -> (int) (rhs.value - lhs.value));

        if (mServerList != null && !mServerList.isEmpty()) {
          server = mServerList.get(0).domainName;
          mCountry = mServerList.get(0).county;
          mIpAddress = mServerList.get(0).ip;
        }
      }

      //Retrieve OpenVPN config file for server
      if (!server.isEmpty()) {
        char[] serverTemp = server.toCharArray();

        for (int i = 0; i < serverTemp.length; i++) {
          if (serverTemp[i] == '.') {
            serverTemp[i] = '_';
          }
        }

        if (mConnectDialog != null && !isFinishing()) {
          mConnectDialog.dismiss();
          mConnectDialog = null;
        }

        mConnectDialog = new ProgressDialog(this);
        mConnectDialog.setTitle(R.string.bc_please_wait);
        mConnectDialog.setMessage(getResources().getString(R.string.bc_getting_config));
        mConnectDialog.setCancelable(false);
        mConnectDialog.setCanceledOnTouchOutside(false);
        mConnectDialog.show();

        String query = "https://blackcap.zone/ovpn/" + String.valueOf(serverTemp) + ".ovpn";
        new BlackcapMain.DownloadWebpageTask().execute(query);
      }
    } else {
      new AlertDialog.Builder(this, R.style.blackcap_Dialog)
          .setTitle(R.string.error)
          .setMessage(getResources().getString(R.string.bc_internet_required))
          .setPositiveButton(android.R.string.ok, null)
          .show();
    }
  }

  /**
   * Disconnect VPN connection
   * @param view
   */
  public void disconnect(View view) {
    Log.d(DEBUG_TAG, this.getClass().getSimpleName());
    if (VpnStatus.isVPNActive()) {
      Intent disconnectVPN = new Intent(this, DisconnectVPN.class);
      startActivity(disconnectVPN);
    }
  }

  /**
   * Change servers if already connected
   * @param view
   */
  public void changeServer(final View view) {
    Log.d(DEBUG_TAG, this.getClass().getSimpleName());
    new AlertDialog.Builder(this, R.style.blackcap_Dialog)
        .setTitle(R.string.bc_change_server_question)
        .setMessage(getResources().getString(R.string.bc_change_server_dialog))
        .setPositiveButton(R.string.bc_change_connection, (dialogInterface, i) -> connect(view))
        .setNegativeButton(android.R.string.no, null)
        .show();
  }

  /**
   * Download from URL
   * @param myurl
   * @return
   * @throws IOException
   */
  private String downloadUrl(String myurl) throws IOException {
    Log.d(DEBUG_TAG, this.getClass().getSimpleName());
    InputStream is = null;
    int len = 8192;

    try {
      URL url = new URL(myurl);
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setReadTimeout(10000 /* milliseconds */);
      conn.setConnectTimeout(15000 /* milliseconds */);
      conn.setRequestMethod("GET");
      conn.setDoInput(true);

      conn.connect();

      is = conn.getInputStream();

      int response = conn.getResponseCode();
      Log.d(DEBUG_TAG, "The response is: " + response);

      return readIt(is, len);
    } finally {
      if (is != null) {
        is.close();
      }
    }
  }

  /**
   * Read data from InputStream
   * @param stream
   * @param len
   * @return
   * @throws IOException
   */
  public String readIt(InputStream stream, int len) throws IOException {
    Log.d(DEBUG_TAG, this.getClass().getSimpleName());
    Reader reader;
    reader = new InputStreamReader(stream, "UTF-8");

    char[] buffer = new char[len];

    int offset = 0;
    while (offset != -1) {
      int temp = reader.read(buffer, offset, len - offset);
      if (temp != -1) {
        offset += temp;
      } else {
        offset = -1;
      }
    }

    int i;
    for (i = 0; i < buffer.length && buffer[i] != 0; ) {
      i++;
    }
    return new String(buffer, 0, i);
  }

  /**
   * Callback for VPN status change
   * @param state
   * @param logMessage
   * @param localizedResId
   * @param level
   */
  @Override
  public void updateState(String state, String logMessage, int localizedResId, VpnStatus.ConnectionStatus level) {
    Log.d(DEBUG_TAG, this.getClass().getSimpleName());
    switch (level) {
      case LEVEL_CONNECTED:
        mConnectionStatus = ConnectionStatus.Connected;

        if (mConnectDialog != null) {
          mConnectDialog.dismiss();
          mConnectDialog = null;
        }

        runOnUiThread(() -> {
          if (mBasicFragment.getView() != null) {
            ((Button) mBasicFragment.getView()
                .findViewById(R.id.button_basic_connect_disconnect)).setText(R.string.bc_disconnect);
            (mBasicFragment.getView()
                .findViewById(R.id.button_basic_connect_disconnect)).setEnabled(true);

            ((TextView) mBasicFragment.getView().findViewById(R.id.textView_location)).setText(mCountry);
            ((TextView) mBasicFragment.getView().findViewById(R.id.textView_ipAddress)).setText(mIpAddress);
          }

          if (mAdvancedFragment.getView() != null) {
            (mAdvancedFragment.getView().findViewById(R.id.button_advanced_disconnect)).setEnabled(true);

            ViewSwitcher view_switcher = mAdvancedFragment.getView().findViewById(R.id.view_switcher);
            if (view_switcher.getNextView() == mAdvancedFragment.getView().findViewById(R.id.button_advanced_change)) {
              view_switcher.showNext();
            }

            ((TextView) mAdvancedFragment.getView().findViewById(R.id.textView_location)).setText(mCountry);
            ((TextView) mAdvancedFragment.getView().findViewById(R.id.textView_ipAddress)).setText(mIpAddress);
          }
        });
        break;
      case LEVEL_VPNPAUSED:
        break;
      case LEVEL_CONNECTING_SERVER_REPLIED:
        break;
      case LEVEL_CONNECTING_NO_SERVER_REPLY_YET:
        break;
      case LEVEL_NONETWORK:
        break;
      case LEVEL_NOTCONNECTED:
        mConnectionStatus = ConnectionStatus.Disconnected;

        if (mConnectDialog != null) {
          mConnectDialog.dismiss();
          mConnectDialog = null;
        }

        mCountry = null;
        mIpAddress = null;

        runOnUiThread(() -> {
          if (mChangeServer) {
            mChangeServer = false;
            connect(findViewById(R.id.button_advanced_change));
          }

          if (mBasicFragment.getView() != null) {
            ((Button) mBasicFragment.getView()
                .findViewById(R.id.button_basic_connect_disconnect)).setText(R.string.bc_connect);
            (mBasicFragment.getView()
                .findViewById(R.id.button_basic_connect_disconnect)).setEnabled(true);

            ((TextView) mBasicFragment.getView().findViewById(R.id.textView_location)).setText("-");
            ((TextView) mBasicFragment.getView().findViewById(R.id.textView_ipAddress)).setText("-");
          }

          if (mAdvancedFragment.getView() != null) {
            ((Button) mAdvancedFragment.getView()
                .findViewById(R.id.button_advanced_connect)).setText(R.string.bc_connect);
            (mAdvancedFragment.getView()
                .findViewById(R.id.button_advanced_connect)).setEnabled(true);

            (mAdvancedFragment.getView().findViewById(R.id.button_advanced_disconnect)).setEnabled(false);

            ViewSwitcher view_switcher = mAdvancedFragment.getView().findViewById(R.id.view_switcher);
            if (view_switcher.getNextView() == mAdvancedFragment.getView()
                .findViewById(R.id.button_advanced_connect)) {
              view_switcher.showNext();
            }

            ((TextView) mAdvancedFragment.getView().findViewById(R.id.textView_location)).setText("-");
            ((TextView) mAdvancedFragment.getView().findViewById(R.id.textView_ipAddress)).setText("-");
          }
        });
        break;
      case LEVEL_START:
        mConnectionStatus = ConnectionStatus.Connecting;

        runOnUiThread(() -> {
          if (mBasicFragment.getView() != null) {
            Button button_basic_connect_disconnect = mBasicFragment.getView()
                .findViewById(R.id.button_basic_connect_disconnect);
            button_basic_connect_disconnect.setText(R.string.bc_connecting);
            button_basic_connect_disconnect.setEnabled(false);
          }

          if (mAdvancedFragment.getView() != null) {
            ((Button) mAdvancedFragment.getView().findViewById(R.id.button_advanced_connect)).setText(R.string.bc_connecting);
            mAdvancedFragment.getView().findViewById(R.id.button_advanced_connect).setEnabled(false);

            (mAdvancedFragment.getView().findViewById(R.id.button_advanced_change)).setVisibility(View.INVISIBLE);

            (mAdvancedFragment.getView().findViewById(R.id.button_advanced_disconnect)).setEnabled(false);

            ViewSwitcher view_switcher = mAdvancedFragment.getView().findViewById(R.id.view_switcher);
            if (view_switcher.getNextView() == mAdvancedFragment.getView().findViewById(R.id.button_advanced_connect)) {
              view_switcher.showNext();
            }
          }
        });
      case LEVEL_AUTH_FAILED:
        break;
      case LEVEL_WAITING_FOR_USER_INPUT:
        break;
      case UNKNOWN_LEVEL:
        break;
    }
  }

  @SuppressLint("StaticFieldLeak")
  private class DownloadWebpageTask extends AsyncTask<String, Void, String> {
    @Override
    protected String doInBackground(String... args) {
      Log.d(DEBUG_TAG, this.getClass().getSimpleName());
      try {
        return downloadUrl(args[0]);
      } catch (IOException e) {
        new AlertDialog.Builder(BlackcapMain.this, R.style.blackcap_Dialog)
            .setTitle(getString(R.string.bc_error))
            .setMessage(getString(R.string.bc_login_unable_connect))
            .setPositiveButton(android.R.string.ok, null);
        //.show();

        return ERROR_GET;
      }
    }

    /**
     * Handle data from URL
     * @param result
     */
    @Override
    protected void onPostExecute(String result) {
      if (isDestroyed()) return;
      Log.d(DEBUG_TAG, this.getClass().getSimpleName());

      if (mConnectDialog != null) {
        mConnectDialog.dismiss();
        mConnectDialog = null;
      }

      if (result.toCharArray()[0] == 'd') {
        FileOutputStream outputStream;

        try {
          outputStream = openFileOutput("temp.ovpn", Context.MODE_PRIVATE);
          outputStream.write(result.getBytes());
          outputStream.close();
          Uri uri = Uri.parse("file://" + getFilesDir().toString() + "/temp.ovpn");

          Intent startImport = new Intent(BlackcapMain.this, ConfigConverter.class);
          startImport.setAction(ConfigConverter.IMPORT_PROFILE);
          startImport.setData(uri);
          startActivityForResult(startImport, IMPORT_PROFILE);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * Ping a server to get latency
   */
  private static class PingTask extends AsyncTask<String, Void, Void> {
    @Override
    protected Void doInBackground(String... args) {
      Log.d(DEBUG_TAG, this.getClass().getSimpleName());
      StringBuilder result = new StringBuilder();
      try {
        Process process = Runtime.getRuntime().exec("/system/bin/ping -c 1 " + args[0]);
        BufferedReader stdInput = new BufferedReader(new InputStreamReader((process.getInputStream())));

        String s;
        while ((s = stdInput.readLine()) != null) {
          result.append(s);
        }
        process.destroy();
      } catch (IOException e) {
        e.printStackTrace();
      }

      Pattern pattern = Pattern.compile(".*time=(\\d+)\\sms.*");
      Matcher matcher = pattern.matcher(result.toString());

      int ping = 9999;
      if (matcher.matches()) {
        System.out.println(matcher.group(0));
        String group = matcher.group(1);
        if (group != null) {
          System.out.println(group);
          ping = Integer.parseInt(group);
        }
      }

      for (Server s : BlackcapMain.mServerList) {
        if (s.ip.equals(args[0])) {
          s.ping = ping;
          break;
        }
      }

      return null;
    }
  }

  public static class BasicFragment extends Fragment {
    public BasicFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View view = inflater.inflate(R.layout.blackcap_fragment_basic, container, false);

      Button button_basic = view.findViewById(R.id.button_basic_connect_disconnect);
      if (BlackcapMain.mConnectionStatus == ConnectionStatus.Disconnected) {
        button_basic.setText(R.string.bc_connect);
        button_basic.setEnabled(true);

      } else if (BlackcapMain.mConnectionStatus == ConnectionStatus.Disconnecting) {
        button_basic.setText(R.string.bc_disconnecting);
        button_basic.setEnabled(false);

      } else if (BlackcapMain.mConnectionStatus == ConnectionStatus.Connected) {
        button_basic.setText(R.string.bc_disconnect);
        button_basic.setEnabled(true);

      } else if (BlackcapMain.mConnectionStatus == ConnectionStatus.Connecting) {
        button_basic.setText(R.string.bc_connecting);
        button_basic.setEnabled(false);
      }

      if (BlackcapMain.mCountry != null && !BlackcapMain.mCountry.isEmpty()) {
        if (!BlackcapMain.mCountry.isEmpty()) {
          ((TextView) view.findViewById(R.id.textView_location)).setText(BlackcapMain.mCountry);
        }

        if (!BlackcapMain.mIpAddress.isEmpty()) {
          ((TextView) view.findViewById(R.id.textView_ipAddress)).setText(BlackcapMain.mIpAddress);
        }
      }

      return view;
    }
  }

  public static class AdvancedFragment extends Fragment {
    public AdvancedFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View view = inflater.inflate(R.layout.blackcap_fragment_advanced, container, false);

      ViewSwitcher view_switcher = view.findViewById(R.id.view_switcher);

      Button button_advanced_connect = view.findViewById(R.id.button_advanced_connect);
      Button button_advanced_change = view.findViewById(R.id.button_advanced_change);
      if (BlackcapMain.mConnectionStatus == ConnectionStatus.Disconnected) {
        button_advanced_connect.setText(R.string.bc_connect);
        button_advanced_connect.setVisibility(View.VISIBLE);
        button_advanced_connect.setEnabled(true);

        if (view_switcher.getNextView() == button_advanced_connect) {
          view_switcher.showNext();
        }
      } else if (BlackcapMain.mConnectionStatus == ConnectionStatus.Disconnecting) {
        button_advanced_connect.setText(R.string.bc_disconnecting);
        button_advanced_connect.setVisibility(View.VISIBLE);
        button_advanced_connect.setEnabled(false);

        if (view_switcher.getNextView() == button_advanced_connect) {
          view_switcher.showNext();
        }
      } else if (BlackcapMain.mConnectionStatus == ConnectionStatus.Connected) {
        if (view_switcher.getNextView() == button_advanced_change) {
          view_switcher.showNext();
        }
      } else if (BlackcapMain.mConnectionStatus == ConnectionStatus.Connecting) {
        button_advanced_connect.setText(R.string.bc_connecting);
        button_advanced_connect.setEnabled(false);

        if (view_switcher.getNextView() == button_advanced_connect) {
          view_switcher.showNext();
        }
      }

      if (BlackcapMain.mConnectionStatus == ConnectionStatus.Disconnected) {
        (view.findViewById(R.id.button_advanced_disconnect)).setEnabled(false);
      } else if (BlackcapMain.mConnectionStatus == ConnectionStatus.Disconnecting) {
        (view.findViewById(R.id.button_advanced_disconnect)).setEnabled(false);
      } else if (BlackcapMain.mConnectionStatus == ConnectionStatus.Connected) {
        (view.findViewById(R.id.button_advanced_disconnect)).setEnabled(true);
      } else if (BlackcapMain.mConnectionStatus == ConnectionStatus.Connecting) {
        (view.findViewById(R.id.button_advanced_disconnect)).setEnabled(false);
      }

      if (BlackcapMain.mServerList != null && BlackcapMain.mServerList.size() != 0) {
        List<String> stringList = new ArrayList<>();
        for (Server s : BlackcapMain.mServerList) {
          stringList.add(s.name);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_spinner_dropdown_item, stringList);

        ((Spinner) view.findViewById(R.id.spinner_server)).setAdapter(adapter);
      }

      if (BlackcapMain.mCountry != null && !BlackcapMain.mCountry.isEmpty()) {
        ((TextView) view.findViewById(R.id.textView_location)).setText(BlackcapMain.mCountry);
      }

      if (BlackcapMain.mCountry != null && !BlackcapMain.mIpAddress.isEmpty()) {
        ((TextView) view.findViewById(R.id.textView_ipAddress)).setText(BlackcapMain.mIpAddress);
      }

      return view;
    }
  }
}
