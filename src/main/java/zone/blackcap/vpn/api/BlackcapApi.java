package zone.blackcap.vpn.api;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import zone.blackcap.vpn.R;
import zone.blackcap.vpn.Server;

public class BlackcapApi {
  private static final String DEBUG_TAG = "Blackcap";

  public enum RequestCode { LOGIN,
                            SERVER_LIST,
                            VPN_CONFIG,
                            PURCHASE,
                            UPDATE_MESSAGE_COUNT }

  public enum RequestType { POST,
                            GET }

  public enum ResponseCode { SUCCESS,
                             CONNECTION_FAILED,
                             INCORRECT_DETAILS,
                             PARSE_FAIL,
                             NO_SUBSCRIPTION,
                             BAD_RESPONSE }

  public void login(ApiResponseListener listener, String username, String password) {
    List<Pair<String, String>> keys = new ArrayList<>();
    keys.add(new Pair<>("u", username));
    keys.add(new Pair<>("p", password));
    ApiRequest request = new ApiRequest(RequestCode.LOGIN, keys);
    new ApiRequestTask(listener).execute(request);
  }

  public void purchase(ApiResponseListener listener, String username, String password, String cardNumber, String expdate, String cvv) {
    List<Pair<String, String>> keys = new ArrayList<>();
    keys.add(new Pair<>("u", username));
    keys.add(new Pair<>("p", password));
    keys.add(new Pair<>("card", cardNumber));
    keys.add(new Pair<>("expdate", expdate));
    keys.add(new Pair<>("cvv", cvv));
    ApiRequest request = new ApiRequest(RequestCode.PURCHASE, keys);
    new ApiRequestTask(listener).execute(request);
  }

  public void getServerList(ApiResponseListener listener) {
    ApiRequest request = new ApiRequest(RequestCode.SERVER_LIST);
    new ApiRequestTask(listener).execute(request);
  }

  public void updateMessageCount(ApiResponseListener listener, String username, String password) {
      List<Pair<String, String>> keys = new ArrayList<>();
      keys.add(new Pair<>("u", username));
      keys.add(new Pair<>("p", password));
      ApiRequest request = new ApiRequest(RequestCode.UPDATE_MESSAGE_COUNT, keys);
      new ApiRequestTask(listener).execute(request);
  }

  public class ApiRequest {
    public RequestCode requestCode;
    public RequestType requestType;
    public List<Pair<String, String>> keys;

    public ApiRequest(RequestCode requestCode, List<Pair<String, String>> keys, RequestType requestType) {
      this.requestCode = requestCode;
      this.keys = keys;
      this.requestType = requestType;
    }

    public ApiRequest(RequestCode requestCode, List<Pair<String, String>> keys) {
      this.requestCode = requestCode;
      this.keys = keys;
      this.requestType = RequestType.POST;
    }

    public ApiRequest(RequestCode requestCode) {
      this.requestCode = requestCode;
      this.keys = null;
      this.requestType = RequestType.GET;
    }
  }

  public class ApiResponse {
    public RequestCode requestCode;
    public ResponseCode code;
    public boolean vpn;
    public boolean messenger;
    public int message_count;
    public String vpnConfig;
    public List<Server> servers;
    public String err;

    ApiResponse(RequestCode requestCode, ResponseCode code) {
      this.requestCode = requestCode;
      this.code = code;
    }

    ApiResponse(RequestCode requestCode, ResponseCode code, boolean vpn, boolean messenger, int message_count) {
      this.requestCode = requestCode;
      this.code = code;
      this.vpn = vpn;
      this.messenger = messenger;
      this.message_count = message_count;
    }

//    ApiResponse(RequestCode requestCode, ResponseCode code, String vpnConfig) {
//      this.requestCode = requestCode;
//      this.code = code;
//      this.vpnConfig = vpnConfig;
//    }

    ApiResponse(RequestCode requestCode, ResponseCode code, List<Server> servers) {
      this.requestCode = requestCode;
      this.code = code;
      this.servers = servers;
    }

    ApiResponse(RequestCode requestCode, ResponseCode code, String err) {
      this.requestCode = requestCode;
      this.code = code;
      this.err = err;
    }
  }

  public interface ApiResponseListener{
    void onTaskCompleted(ApiResponse apiResponse);
  }

  public class ApiRequestTask extends AsyncTask<ApiRequest, Void, ApiResponse> {
    private ApiResponseListener listener;

    public ApiRequestTask(ApiResponseListener listener){
      this.listener = listener;
    }

    protected ApiResponse doInBackground(ApiRequest... args) {
      int responseCode = 0;
      String responseData = "";

      String urlString = "";
      if (args[0].requestCode == RequestCode.LOGIN) {
        urlString = "https://blackcap.zone/api/login.php";
      } else if (args[0].requestCode == RequestCode.PURCHASE) {
        urlString = "https://blackcap.zone/api/upgrade-product.php";
      } else if (args[0].requestCode == RequestCode.SERVER_LIST) {
        urlString = "https://blackcap.zone/api/status-list.php";
      } else if (args[0].requestCode == RequestCode.UPDATE_MESSAGE_COUNT) {
        urlString = "https://blackcap.zone/api/message-sent.php";
      }

      try {
        URL url = new URL(urlString);

        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        conn.setReadTimeout(80000 /* milliseconds */);
        conn.setConnectTimeout(80000 /* milliseconds */);
        conn.setDoInput(true);
        conn.setDoOutput(true);

        if (args[0].requestType == RequestType.POST) {
          conn.setRequestMethod("POST");

          StringBuilder keyBuilder = new StringBuilder();

          for (int i = 0; i < args[0].keys.size(); i++)
          {
            if (i != 0) {
              keyBuilder.append("&");
            }
            keyBuilder.append(args[0].keys.get(i).first);
            keyBuilder.append("=");
            keyBuilder.append(URLEncoder.encode(args[0].keys.get(i).second, "UTF-8"));
          }

          OutputStream os = new BufferedOutputStream(conn.getOutputStream());
          os.write(keyBuilder.toString().getBytes());
          os.flush();
          os.close();
        } else if (args[0].requestType == RequestType.GET) {
          conn.setRequestMethod("GET");
        }

        responseCode = conn.getResponseCode();
        Log.d(DEBUG_TAG, "The response code is: " + responseCode);

        String line;
        BufferedReader br;
        if (conn.getResponseCode() < HttpsURLConnection.HTTP_BAD_REQUEST) {
          br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        } else {
          br = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        }
        while ((line = br.readLine()) != null) {
          responseData += line;
        }
      } catch (Exception e) {
        Writer writer = new StringWriter();
        e.printStackTrace(new PrintWriter(writer));
        String s = writer.toString();
//        e.printStackTrace();

        return new ApiResponse(args[0].requestCode, ResponseCode.CONNECTION_FAILED, s);
      }

      Log.d(DEBUG_TAG, "The response data is: " + responseData);
      return parseApiResponse(args[0], responseCode, responseData);
    }

    protected void onPostExecute(ApiResponse apiResponse){
      listener.onTaskCompleted(apiResponse);
    }

    private ApiResponse parseApiResponse(ApiRequest apiRequest, int responseCode, String responseData) {
      if (apiRequest.requestCode == RequestCode.LOGIN) {
        return parseApiResponseLogin(apiRequest, responseCode, responseData);
      } else if (apiRequest.requestCode == RequestCode.SERVER_LIST) {
        return parseApiResponseServerlist(apiRequest, responseCode, responseData);
      } else if (apiRequest.requestCode == RequestCode.VPN_CONFIG) {
        return parseApiResponseVpnconfig(apiRequest, responseCode, responseData);
      } else if (apiRequest.requestCode == RequestCode.PURCHASE) {
        return parseApiResponsePurchase(apiRequest, responseCode, responseData);
      } else if (apiRequest.requestCode == RequestCode.UPDATE_MESSAGE_COUNT) {
        //return parseApiResponseLogin(apiRequest, responseCode, responseData);
        return new ApiResponse(apiRequest.requestCode, ResponseCode.SUCCESS);
      }

      return new ApiResponse(apiRequest.requestCode, ResponseCode.BAD_RESPONSE);
    }

    private ApiResponse parseApiResponseLogin(ApiRequest apiRequest, int responseCode, String responseData) {
      if (responseCode == 402) {
        return new ApiResponse(apiRequest.requestCode, ResponseCode.NO_SUBSCRIPTION);
      } else if (responseCode == 403) {
        return new ApiResponse(apiRequest.requestCode, ResponseCode.INCORRECT_DETAILS);
      }

      try {
        JSONObject main = new JSONObject(responseData);

        JSONObject meta = main.getJSONObject("meta");
        int code = meta.getInt("code");

        if (code == 200) {
          JSONObject data = main.getJSONObject("data");
          boolean vpn = data.getBoolean("vpn");
          boolean messenger = data.getBoolean("messenger");
          int message_count = messenger ? 0 : data.getInt("message_count");

          return new ApiResponse(apiRequest.requestCode, ResponseCode.SUCCESS, vpn, messenger, message_count);
        } else if (code == 402) {
          return new ApiResponse(apiRequest.requestCode, ResponseCode.NO_SUBSCRIPTION);
        } else if (code == 403) {
          return new ApiResponse(apiRequest.requestCode, ResponseCode.INCORRECT_DETAILS);
        }
      } catch (Exception e) {
        Log.e(DEBUG_TAG, e.toString());
      }

      return new ApiResponse(apiRequest.requestCode, ResponseCode.PARSE_FAIL);
    }

    private ApiResponse parseApiResponseServerlist(ApiRequest apiRequest, int responseCode, String responseData) {
      if (responseCode != 200) {
        return new ApiResponse(apiRequest.requestCode, ResponseCode.BAD_RESPONSE);
      }

      try {
        JSONObject main = new JSONObject(responseData);

        JSONObject meta = main.getJSONObject("meta");
        int code = meta.getInt("code");

        if (code == 200) {
          List<Server> serverList = new ArrayList<>();

          JSONObject data = main.getJSONObject("data");

          JSONArray workers = data.getJSONArray("workers");

          for (int i = 0; i < workers.length(); i++) {
            JSONObject serverObject = workers.getJSONObject(i);

            Server server = new Server();
            server.name = serverObject.getString("name");
            server.name = serverObject.getString("name");
            server.ip = serverObject.getString("ip");
            server.domainName = serverObject.getString("domainName");
            server.county = serverObject.getString("country");
            server.city = serverObject.getString("city");

            server.up = serverObject.getBoolean("up");

            server.weight = serverObject.getInt("weight");
            server.currentLoad = serverObject.getInt("currentLoad");
            server.loadPercentage = serverObject.getInt("loadPercentage");

            server.canTorrent = serverObject.getBoolean("canTorrent");
            server.supportsPPTP = serverObject.getBoolean("supportsPPTP");
            server.supportsSSTP = serverObject.getBoolean("supportsSSTP");
            server.supportsL2TP = serverObject.getBoolean("supportsL2TP");
            server.supportsOpenVPN = serverObject.getBoolean("supportsOpenVPN");
            server.supportsSoftEther = serverObject.getBoolean("supportsSoftEther");

            server.ping = 9999;
            server.value = 0;

            serverList.add(server);
          }

          return new ApiResponse(apiRequest.requestCode, ResponseCode.SUCCESS, serverList);
        } else {
          return new ApiResponse(apiRequest.requestCode, ResponseCode.BAD_RESPONSE);
        }
      } catch (Exception e) {
        Log.e(DEBUG_TAG, e.toString());
      }

      return new ApiResponse(apiRequest.requestCode, ResponseCode.PARSE_FAIL);
    }

    private ApiResponse parseApiResponseVpnconfig(ApiRequest apiRequest, int responseCode, String responseData) {
      return new ApiResponse(apiRequest.requestCode, ResponseCode.PARSE_FAIL);
    }

    private ApiResponse parseApiResponsePurchase(ApiRequest apiRequest, int responseCode, String responseData) {
      if (responseCode != 200) {
        return new ApiResponse(apiRequest.requestCode, ResponseCode.BAD_RESPONSE);
      }
      return new ApiResponse(apiRequest.requestCode, ResponseCode.SUCCESS);
    }
  }
}
