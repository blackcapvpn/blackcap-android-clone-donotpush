package zone.blackcap.vpn.dependencies;

import android.content.Context;

import zone.blackcap.vpn.gcm.GcmBroadcastReceiver;
import zone.blackcap.vpn.logging.Log;

import org.greenrobot.eventbus.EventBus;
import zone.blackcap.vpn.BuildConfig;
import zone.blackcap.vpn.CreateProfileActivity;
import zone.blackcap.vpn.DeviceListFragment;
import zone.blackcap.vpn.crypto.storage.SignalProtocolStoreImpl;
import zone.blackcap.vpn.events.ReminderUpdateEvent;
import zone.blackcap.vpn.jobs.AttachmentDownloadJob;
import zone.blackcap.vpn.jobs.AvatarDownloadJob;
import zone.blackcap.vpn.jobs.CleanPreKeysJob;
import zone.blackcap.vpn.jobs.CreateSignedPreKeyJob;
import zone.blackcap.vpn.jobs.GcmRefreshJob;
import zone.blackcap.vpn.jobs.MultiDeviceBlockedUpdateJob;
import zone.blackcap.vpn.jobs.MultiDeviceContactUpdateJob;
import zone.blackcap.vpn.jobs.MultiDeviceGroupUpdateJob;
import zone.blackcap.vpn.jobs.MultiDeviceProfileKeyUpdateJob;
import zone.blackcap.vpn.jobs.MultiDeviceReadReceiptUpdateJob;
import zone.blackcap.vpn.jobs.MultiDeviceReadUpdateJob;
import zone.blackcap.vpn.jobs.MultiDeviceVerifiedUpdateJob;
import zone.blackcap.vpn.jobs.PushGroupSendJob;
import zone.blackcap.vpn.jobs.PushGroupUpdateJob;
import zone.blackcap.vpn.jobs.PushMediaSendJob;
import zone.blackcap.vpn.jobs.PushNotificationReceiveJob;
import zone.blackcap.vpn.jobs.PushTextSendJob;
import zone.blackcap.vpn.jobs.RefreshAttributesJob;
import zone.blackcap.vpn.jobs.RefreshPreKeysJob;
import zone.blackcap.vpn.jobs.RequestGroupInfoJob;
import zone.blackcap.vpn.jobs.RetrieveProfileAvatarJob;
import zone.blackcap.vpn.jobs.RetrieveProfileJob;
import zone.blackcap.vpn.jobs.RotateSignedPreKeyJob;
import zone.blackcap.vpn.jobs.SendReadReceiptJob;
import zone.blackcap.vpn.preferences.AppProtectionPreferenceFragment;
import zone.blackcap.vpn.push.SecurityEventListener;
import zone.blackcap.vpn.push.SignalServiceNetworkAccess;
import zone.blackcap.vpn.service.MessageRetrievalService;
import zone.blackcap.vpn.service.WebRtcCallService;
import zone.blackcap.vpn.util.TextSecurePreferences;
import org.whispersystems.libsignal.util.guava.Optional;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.SignalServiceMessageReceiver;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.util.CredentialsProvider;
import org.whispersystems.signalservice.api.util.RealtimeSleepTimer;
import org.whispersystems.signalservice.api.util.SleepTimer;
import org.whispersystems.signalservice.api.util.UptimeSleepTimer;
import org.whispersystems.signalservice.api.websocket.ConnectivityListener;

import dagger.Module;
import dagger.Provides;

@Module(complete = false, injects = {CleanPreKeysJob.class,
                                     CreateSignedPreKeyJob.class,
                                     PushGroupSendJob.class,
                                     PushTextSendJob.class,
                                     PushMediaSendJob.class,
                                     AttachmentDownloadJob.class,
                                     RefreshPreKeysJob.class,
                                     MessageRetrievalService.class,
                                     PushNotificationReceiveJob.class,
                                     MultiDeviceContactUpdateJob.class,
                                     MultiDeviceGroupUpdateJob.class,
                                     MultiDeviceReadUpdateJob.class,
                                     MultiDeviceBlockedUpdateJob.class,
                                     DeviceListFragment.class,
                                     RefreshAttributesJob.class,
                                     GcmRefreshJob.class,
                                     RequestGroupInfoJob.class,
                                     PushGroupUpdateJob.class,
                                     AvatarDownloadJob.class,
                                     RotateSignedPreKeyJob.class,
                                     WebRtcCallService.class,
                                     RetrieveProfileJob.class,
                                     MultiDeviceVerifiedUpdateJob.class,
                                     CreateProfileActivity.class,
                                     RetrieveProfileAvatarJob.class,
                                     MultiDeviceProfileKeyUpdateJob.class,
                                     SendReadReceiptJob.class,
                                     MultiDeviceReadReceiptUpdateJob.class,
                                     AppProtectionPreferenceFragment.class,
                                     GcmBroadcastReceiver.class})
public class SignalCommunicationModule {

  private static final String TAG = SignalCommunicationModule.class.getSimpleName();

  private final Context                      context;
  private final SignalServiceNetworkAccess   networkAccess;

  private SignalServiceAccountManager  accountManager;
  private SignalServiceMessageSender   messageSender;
  private SignalServiceMessageReceiver messageReceiver;

  public SignalCommunicationModule(Context context, SignalServiceNetworkAccess networkAccess) {
    this.context       = context;
    this.networkAccess = networkAccess;
  }

  @Provides
  synchronized SignalServiceAccountManager provideSignalAccountManager() {
    if (this.accountManager == null) {
      this.accountManager = new SignalServiceAccountManager(networkAccess.getConfiguration(context),
                                                            new DynamicCredentialsProvider(context),
                                                            BuildConfig.USER_AGENT);
    }

    return this.accountManager;
  }

  @Provides
  synchronized SignalServiceMessageSender provideSignalMessageSender() {
    if (this.messageSender == null) {
      this.messageSender = new SignalServiceMessageSender(networkAccess.getConfiguration(context),
                                                          new DynamicCredentialsProvider(context),
                                                          new SignalProtocolStoreImpl(context),
                                                          BuildConfig.USER_AGENT,
                                                          Optional.fromNullable(MessageRetrievalService.getPipe()),
                                                          Optional.of(new SecurityEventListener(context)));
    } else {
      this.messageSender.setMessagePipe(MessageRetrievalService.getPipe());
    }

    return this.messageSender;
  }

  @Provides
  synchronized SignalServiceMessageReceiver provideSignalMessageReceiver() {
    if (this.messageReceiver == null) {
      SleepTimer sleepTimer =  TextSecurePreferences.isGcmDisabled(context) ? new RealtimeSleepTimer(context) : new UptimeSleepTimer();

      this.messageReceiver = new SignalServiceMessageReceiver(networkAccess.getConfiguration(context),
                                                              new DynamicCredentialsProvider(context),
                                                              BuildConfig.USER_AGENT,
                                                              new PipeConnectivityListener(),
                                                              sleepTimer);
    }

    return this.messageReceiver;
  }

  private static class DynamicCredentialsProvider implements CredentialsProvider {

    private final Context context;

    private DynamicCredentialsProvider(Context context) {
      this.context = context.getApplicationContext();
    }

    @Override
    public String getUser() {
      return TextSecurePreferences.getLocalNumber(context);
    }

    @Override
    public String getPassword() {
      return TextSecurePreferences.getPushServerPassword(context);
    }

    @Override
    public String getSignalingKey() {
      return TextSecurePreferences.getSignalingKey(context);
    }
  }

  private class PipeConnectivityListener implements ConnectivityListener {

    @Override
    public void onConnected() {
      Log.i(TAG, "onConnected()");
    }

    @Override
    public void onConnecting() {
      Log.i(TAG, "onConnecting()");
    }

    @Override
    public void onDisconnected() {
      Log.w(TAG, "onDisconnected()");
    }

    @Override
    public void onAuthenticationFailure() {
      Log.w(TAG, "onAuthenticationFailure()");
      TextSecurePreferences.setUnauthorizedReceived(context, true);
      EventBus.getDefault().post(new ReminderUpdateEvent());
    }

  }

}
