package zone.blackcap.vpn.fragments;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import zone.blackcap.vpn.R;

public class NavigationAdapter extends BaseAdapter {
    private Activity activity;
    private static LayoutInflater inflater = null;
    private String[] mNames;
    private int[] mImages;

    public NavigationAdapter(Activity a, String[] text1, int[] imageIds) {
        activity = a;
        mNames = text1;
        mImages = imageIds;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mNames.length;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.blackcap_drawer_item, null);

        TextView name = (TextView) vi.findViewById(R.id.textView_item);
        ImageView icon = (ImageView) vi.findViewById(R.id.imageView_item);

        name.setText(mNames[position]);
        icon.setImageResource(mImages[position]);

        return vi;
    }
}
