package zone.blackcap.vpn;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;

import java.util.Locale;

import zone.blackcap.vpn.core.VpnStatus;
import zone.blackcap.vpn.logging.Log;
import zone.blackcap.vpn.util.TextSecurePreferences;

public class BlackcapSplash extends Activity {
  private static final String TAG = "BlackcapSplash";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.blackcap_splash);

    new Startup(this).execute();
  }

  public void setSelectedLocale() {
    SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
    String lang = sharedPreferences.getString("current_language", "English").equals("English") ? "en" : "es";
    Log.i(TAG, lang);

    TextSecurePreferences.setLanguage(this, lang);
    Locale myLocale = new Locale(lang);
    Locale.setDefault(myLocale);
    Resources res = getResources();
    DisplayMetrics dm = res.getDisplayMetrics();
    android.content.res.Configuration conf = res.getConfiguration();
    conf.locale = myLocale;
    res.updateConfiguration(conf, dm);
  }

  private class Startup extends AsyncTask<Void, Void, Void> {
    private Context mContext;

    public Startup(Context context) {
      mContext = context;
    }

    protected Void doInBackground(Void... params) {
      try
      {
        setSelectedLocale();
        Thread.sleep(1000);
      } catch (InterruptedException e) {

      }

      if (!VpnStatus.isVPNActive()) {
        Intent intent = new Intent(BlackcapSplash.this, BlackcapLogin.class);
        startActivity(intent);
      } else {
        Intent intent = new Intent(BlackcapSplash.this, BlackcapMain.class);
        startActivity(intent);
      }
      ((Activity)mContext).finish();

      return null;
    }
  }
}
