package zone.blackcap.vpn;

import android.app.AlertDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class BlackcapLicence extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.blackcap_licence);

    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(true);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        actionBar.setElevation(0.0f);
      }
    }
  }


  public void openAbout(View view) {
    new AlertDialog.Builder(this, R.style.blackcap_Dialog)
        .setTitle(getString(R.string.settings_about))
        .setMessage(getString(R.string.bc_license_thank_you_using) + BuildConfig.VERSION_NAME + getString(R.string.bc_license_rights))
        .setPositiveButton(android.R.string.ok, null)
        .show();
  }
}
