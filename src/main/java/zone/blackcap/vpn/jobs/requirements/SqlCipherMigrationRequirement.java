package zone.blackcap.vpn.jobs.requirements;


import android.content.Context;
import android.support.annotation.NonNull;

import zone.blackcap.vpn.jobmanager.dependencies.ContextDependent;
import zone.blackcap.vpn.jobmanager.requirements.Requirement;
import zone.blackcap.vpn.jobmanager.requirements.SimpleRequirement;
import zone.blackcap.vpn.util.TextSecurePreferences;

public class SqlCipherMigrationRequirement extends SimpleRequirement implements ContextDependent {

  @SuppressWarnings("unused")
  private static final String TAG = SqlCipherMigrationRequirement.class.getSimpleName();

  private transient Context context;

  public SqlCipherMigrationRequirement(@NonNull Context context) {
    this.context = context;
  }

  @Override
  public void setContext(Context context) {
    this.context = context;
  }

  @Override
  public boolean isPresent() {
    return !TextSecurePreferences.getNeedsSqlCipherMigration(context);
  }
}
