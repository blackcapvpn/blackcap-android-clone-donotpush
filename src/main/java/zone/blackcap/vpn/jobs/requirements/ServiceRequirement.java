package zone.blackcap.vpn.jobs.requirements;

import android.content.Context;

import zone.blackcap.vpn.jobmanager.dependencies.ContextDependent;
import zone.blackcap.vpn.jobmanager.requirements.Requirement;
import zone.blackcap.vpn.jobmanager.requirements.SimpleRequirement;
import zone.blackcap.vpn.sms.TelephonyServiceState;

public class ServiceRequirement extends SimpleRequirement implements ContextDependent {

  private static final String TAG = ServiceRequirement.class.getSimpleName();

  private transient Context context;

  public ServiceRequirement(Context context) {
    this.context  = context;
  }

  @Override
  public void setContext(Context context) {
    this.context = context;
  }

  @Override
  public boolean isPresent() {
    TelephonyServiceState telephonyServiceState = new TelephonyServiceState();
    return telephonyServiceState.isConnected(context);
  }
}
