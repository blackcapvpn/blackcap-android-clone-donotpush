package zone.blackcap.vpn.jobs.requirements;

import android.content.Context;

import zone.blackcap.vpn.jobmanager.dependencies.ContextDependent;
import zone.blackcap.vpn.jobmanager.requirements.NetworkRequirement;
import zone.blackcap.vpn.jobmanager.requirements.Requirement;
import zone.blackcap.vpn.jobmanager.requirements.SimpleRequirement;

public class NetworkOrServiceRequirement extends SimpleRequirement implements ContextDependent {

  private transient Context context;

  public NetworkOrServiceRequirement(Context context) {
    this.context = context;
  }

  @Override
  public void setContext(Context context) {
    this.context = context;
  }

  @Override
  public boolean isPresent() {
    NetworkRequirement networkRequirement = new NetworkRequirement(context);
    ServiceRequirement serviceRequirement = new ServiceRequirement(context);

    return networkRequirement.isPresent() || serviceRequirement.isPresent();
  }
}
