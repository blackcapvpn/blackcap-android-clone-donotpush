package zone.blackcap.vpn.jobs.requirements;

import android.content.Context;

import zone.blackcap.vpn.jobmanager.dependencies.ContextDependent;
import zone.blackcap.vpn.jobmanager.requirements.Requirement;
import zone.blackcap.vpn.jobmanager.requirements.SimpleRequirement;
import zone.blackcap.vpn.service.KeyCachingService;

public class MasterSecretRequirement extends SimpleRequirement implements ContextDependent {

  private transient Context context;

  public MasterSecretRequirement(Context context) {
    this.context = context;
  }

  @Override
  public boolean isPresent() {
    return KeyCachingService.getMasterSecret(context) != null;
  }

  @Override
  public void setContext(Context context) {
    this.context = context;
  }
}
