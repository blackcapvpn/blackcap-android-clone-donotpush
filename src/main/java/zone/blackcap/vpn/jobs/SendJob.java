package zone.blackcap.vpn.jobs;

import android.content.Context;
import android.support.annotation.NonNull;

import zone.blackcap.vpn.BuildConfig;
import zone.blackcap.vpn.R;
import zone.blackcap.vpn.TextSecureExpiredException;
import zone.blackcap.vpn.attachments.Attachment;
import zone.blackcap.vpn.crypto.MasterSecret;
import zone.blackcap.vpn.database.AttachmentDatabase;
import zone.blackcap.vpn.database.DatabaseFactory;
import zone.blackcap.vpn.jobmanager.JobParameters;
import zone.blackcap.vpn.logging.Log;
import zone.blackcap.vpn.mms.MediaConstraints;
import zone.blackcap.vpn.mms.MediaStream;
import zone.blackcap.vpn.mms.MmsException;
import zone.blackcap.vpn.transport.UndeliverableMessageException;
import zone.blackcap.vpn.util.MediaUtil;
import zone.blackcap.vpn.util.Util;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public abstract class SendJob extends MasterSecretJob {

  @SuppressWarnings("unused")
  private final static String TAG = SendJob.class.getSimpleName();

  public SendJob(Context context, JobParameters parameters) {
    super(context, parameters);
  }

  @Override
  protected String getDescription() {
    return context.getString(R.string.SendJob_sending_a_message);
  }

  @Override
  public final void onRun(MasterSecret masterSecret) throws Exception {
    if (Util.getDaysTillBuildExpiry() <= 0) {
      throw new TextSecureExpiredException(String.format("TextSecure expired (build %d, now %d)",
                                                         BuildConfig.BUILD_TIMESTAMP,
                                                         System.currentTimeMillis()));
    }

    Log.i(TAG, "Starting message send attempt");
    onSend(masterSecret);
    Log.i(TAG, "Message send completed");
  }

  protected abstract void onSend(MasterSecret masterSecret) throws Exception;

  protected void markAttachmentsUploaded(long messageId, @NonNull List<Attachment> attachments) {
    AttachmentDatabase database = DatabaseFactory.getAttachmentDatabase(context);

    for (Attachment attachment : attachments) {
      database.markAttachmentUploaded(messageId, attachment);
    }
  }

  protected List<Attachment> scaleAndStripExifFromAttachments(@NonNull MediaConstraints constraints,
                                                              @NonNull List<Attachment> attachments)
      throws UndeliverableMessageException
  {
    AttachmentDatabase attachmentDatabase = DatabaseFactory.getAttachmentDatabase(context);
    List<Attachment>   results            = new LinkedList<>();

    for (Attachment attachment : attachments) {
      try {
        if (constraints.isSatisfied(context, attachment)) {
          if (MediaUtil.isJpeg(attachment)) {
            MediaStream stripped = constraints.getResizedMedia(context, attachment);
            results.add(attachmentDatabase.updateAttachmentData(attachment, stripped));
          } else {
            results.add(attachment);
          }
        } else if (constraints.canResize(attachment)) {
          MediaStream resized = constraints.getResizedMedia(context, attachment);
          results.add(attachmentDatabase.updateAttachmentData(attachment, resized));
        } else {
          throw new UndeliverableMessageException("Size constraints could not be met!");
        }
      } catch (IOException | MmsException e) {
        throw new UndeliverableMessageException(e);
      }
    }

    return results;
  }
}
