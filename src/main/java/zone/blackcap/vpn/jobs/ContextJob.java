package zone.blackcap.vpn.jobs;

import android.content.Context;

import zone.blackcap.vpn.jobmanager.Job;
import zone.blackcap.vpn.jobmanager.JobParameters;
import zone.blackcap.vpn.jobmanager.dependencies.ContextDependent;

public abstract class ContextJob extends Job implements ContextDependent {

  protected transient Context context;

  protected ContextJob(Context context, JobParameters parameters) {
    super(parameters);
    this.context = context;
  }

  public void setContext(Context context) {
    this.context = context;
  }

  protected Context getContext() {
    return context;
  }
}
