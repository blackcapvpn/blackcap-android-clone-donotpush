package zone.blackcap.vpn.jobs;


import android.content.Context;
import android.support.annotation.NonNull;

import zone.blackcap.vpn.ApplicationContext;
import zone.blackcap.vpn.crypto.IdentityKeyUtil;
import zone.blackcap.vpn.crypto.MasterSecret;
import zone.blackcap.vpn.crypto.PreKeyUtil;
import zone.blackcap.vpn.dependencies.InjectableType;
import zone.blackcap.vpn.jobmanager.JobParameters;
import zone.blackcap.vpn.jobmanager.SafeData;
import zone.blackcap.vpn.logging.Log;
import zone.blackcap.vpn.util.TextSecurePreferences;
import org.whispersystems.libsignal.IdentityKeyPair;
import org.whispersystems.libsignal.state.SignedPreKeyRecord;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

import javax.inject.Inject;

import androidx.work.Data;

public class RotateSignedPreKeyJob extends MasterSecretJob implements InjectableType {

  private static final String TAG = RotateSignedPreKeyJob.class.getName();

  @Inject transient SignalServiceAccountManager accountManager;

  public RotateSignedPreKeyJob() {
    super(null, null);
  }

  public RotateSignedPreKeyJob(Context context) {
    super(context, JobParameters.newBuilder()
                                .withNetworkRequirement()
                                .withMasterSecretRequirement()
                                .withRetryCount(5)
                                .create());
  }

  @Override
  protected void initialize(@NonNull SafeData data) {
  }

  @Override
  protected @NonNull Data serialize(@NonNull Data.Builder dataBuilder) {
    return dataBuilder.build();
  }

  @Override
  public void onRun(MasterSecret masterSecret) throws Exception {
    Log.i(TAG, "Rotating signed prekey...");

    IdentityKeyPair    identityKey        = IdentityKeyUtil.getIdentityKeyPair(context);
    SignedPreKeyRecord signedPreKeyRecord = PreKeyUtil.generateSignedPreKey(context, identityKey, false);

    accountManager.setSignedPreKey(signedPreKeyRecord);

    PreKeyUtil.setActiveSignedPreKeyId(context, signedPreKeyRecord.getId());
    TextSecurePreferences.setSignedPreKeyRegistered(context, true);
    TextSecurePreferences.setSignedPreKeyFailureCount(context, 0);

    ApplicationContext.getInstance(context)
                      .getJobManager()
                      .add(new CleanPreKeysJob(context));
  }

  @Override
  public boolean onShouldRetryThrowable(Exception exception) {
    return exception instanceof PushNetworkException;
  }

  @Override
  public void onCanceled() {
    TextSecurePreferences.setSignedPreKeyFailureCount(context, TextSecurePreferences.getSignedPreKeyFailureCount(context) + 1);
  }
}
