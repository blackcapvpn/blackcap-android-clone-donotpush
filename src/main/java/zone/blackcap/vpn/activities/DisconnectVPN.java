/*
 * Copyright (c) 2012-2016 Arne Schwabe
 * Distributed under the GNU GPL v2 with additional terms. For full terms see the file doc/LICENSE.txt
 */

package zone.blackcap.vpn.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.*;
import android.os.IBinder;

import zone.blackcap.vpn.LaunchVPN;
import zone.blackcap.vpn.R;
import zone.blackcap.vpn.VpnProfile;
import zone.blackcap.vpn.core.OpenVPNService;
import zone.blackcap.vpn.core.ProfileManager;

/**
 * Created by arne on 13.10.13.
 */
public class DisconnectVPN extends Activity implements DialogInterface.OnClickListener, DialogInterface.OnCancelListener {
    protected OpenVPNService mService;

    private ServiceConnection mConnection = new ServiceConnection() {


        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            OpenVPNService.LocalBinder binder = (OpenVPNService.LocalBinder) service;
            mService = binder.getService();

            SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
            if (!sharedPreferences.getBoolean("disconnect_dialog", true)) {
                ProfileManager.setConntectedVpnProfileDisconnected(DisconnectVPN.this);
                if (mService != null && mService.getManagement() != null)
                    mService.getManagement().stopVPN(false);

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("disconnect_dialog", true);
                editor.apply();
                DisconnectVPN.this.finish();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
        }

    };

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(this, OpenVPNService.class);
        intent.setAction(OpenVPNService.START_SERVICE);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

        SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
        if (sharedPreferences.getBoolean("disconnect_dialog", true)) {
            showDisconnectDialog();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(mConnection);
    }

    private void showDisconnectDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Become visible?");
        builder.setMessage("Make sure to close any apps you want to remain private before reconnecting to a regular network.");
        builder.setNegativeButton(android.R.string.cancel, this);
        builder.setPositiveButton("Become Visible", this);
        builder.setOnCancelListener(this);

        builder.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        VpnProfile lastVPN = ProfileManager.getLastConnectedVpn();
        if (which == DialogInterface.BUTTON_POSITIVE) {
            ProfileManager.setConntectedVpnProfileDisconnected(this);
            if (mService != null && mService.getManagement() != null)
                mService.getManagement().stopVPN(false);
        } else if (which == DialogInterface.BUTTON_NEUTRAL && lastVPN != null) {
            Intent intent = new Intent(this, LaunchVPN.class);
            intent.putExtra(LaunchVPN.EXTRA_KEY, lastVPN.getUUID().toString());
            intent.setAction(Intent.ACTION_MAIN);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        finish();
    }
}
