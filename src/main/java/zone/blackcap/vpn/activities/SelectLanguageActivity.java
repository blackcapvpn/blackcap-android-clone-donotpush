package zone.blackcap.vpn.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.CheckBox;

import java.util.Locale;

import zone.blackcap.vpn.BuildConfig;
import zone.blackcap.vpn.R;
import zone.blackcap.vpn.util.TextSecurePreferences;

public class SelectLanguageActivity extends AppCompatActivity {
  CheckBox cbEnglish, cbSpanish;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_select_language);

    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(true);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        actionBar.setElevation(0.0f);
      }
    }

    cbEnglish = findViewById(R.id.cb_english);
    cbSpanish = findViewById(R.id.cb_spanish);

    updateCheckBox();
  }

  public void onEnglish(View view) {
    changeLanguage("English");
    setLocale("en");
  }

  public void onSpanish(View view) {
    changeLanguage("Spanish");
    setLocale("es");
  }

  public void changeLanguage(String country) {
    SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
    sharedPreferences.edit().putString("current_language", country).apply();

    updateCheckBox();
  }

  public void updateCheckBox() {
    SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);

    cbEnglish.setChecked(sharedPreferences.getString("current_language", "English").equals("English"));
    cbSpanish.setChecked(sharedPreferences.getString("current_language", "English").equals("Spanish"));
  }

  public void setLocale(String lang) {
    TextSecurePreferences.setLanguage(this, lang);
    Locale myLocale = new Locale(lang);
    Locale.setDefault(myLocale);
    Resources res = getResources();
    DisplayMetrics dm = res.getDisplayMetrics();
    Configuration conf = res.getConfiguration();
    conf.locale = myLocale;
    res.updateConfiguration(conf, dm);
  }
}
