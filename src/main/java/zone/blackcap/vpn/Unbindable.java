package zone.blackcap.vpn;

public interface Unbindable {
  public void unbind();
}
