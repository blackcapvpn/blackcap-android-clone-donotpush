package zone.blackcap.vpn.qr;

public interface ScanListener {
  public void onQrDataFound(String data);
}
