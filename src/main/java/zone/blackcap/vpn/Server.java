package zone.blackcap.vpn;

public class Server {
    public String name;
    public String ip;
    public String domainName;
    public String county;
    public String city;
    public boolean up = false;
    public int weight;
    public int currentLoad;
    public int loadPercentage;
    public boolean canTorrent;
    public boolean supportsPPTP;
    public boolean supportsSSTP;
    public boolean supportsL2TP;
    public boolean supportsOpenVPN;
    public boolean supportsSoftEther;
    public int ping = 9999;
    public float value = 0;
}