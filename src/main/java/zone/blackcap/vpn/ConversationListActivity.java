/*
 * Copyright (C) 2014-2017 Open Whisper Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package zone.blackcap.vpn;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.TooltipCompat;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;

import zone.blackcap.vpn.activities.DisconnectVPN;
import zone.blackcap.vpn.activities.SelectLanguageActivity;
import zone.blackcap.vpn.color.MaterialColor;
import zone.blackcap.vpn.components.RatingManager;
import zone.blackcap.vpn.components.SearchToolbar;
import zone.blackcap.vpn.contacts.avatars.ContactColors;
import zone.blackcap.vpn.contacts.avatars.GeneratedContactPhoto;
import zone.blackcap.vpn.contacts.avatars.ProfileContactPhoto;
import zone.blackcap.vpn.database.Address;
import zone.blackcap.vpn.database.DatabaseFactory;
import zone.blackcap.vpn.database.MessagingDatabase.MarkedMessageInfo;
import zone.blackcap.vpn.fragments.NavigationDrawerMessagingFragment;
import zone.blackcap.vpn.lock.RegistrationLockDialog;
import zone.blackcap.vpn.mms.GlideApp;
import zone.blackcap.vpn.notifications.MarkReadReceiver;
import zone.blackcap.vpn.notifications.MessageNotifier;
import zone.blackcap.vpn.permissions.Permissions;
import zone.blackcap.vpn.recipients.Recipient;
import zone.blackcap.vpn.search.SearchFragment;
import zone.blackcap.vpn.service.KeyCachingService;
import zone.blackcap.vpn.util.DynamicLanguage;
import zone.blackcap.vpn.util.DynamicNoActionBarTheme;
import zone.blackcap.vpn.util.DynamicTheme;
import zone.blackcap.vpn.util.TextSecurePreferences;
import org.whispersystems.libsignal.util.guava.Optional;

import java.util.List;

public class ConversationListActivity extends PassphraseRequiredActionBarActivity
    implements ConversationListFragment.ConversationSelectedListener,
               NavigationDrawerMessagingFragment.NavigationDrawerCallbacks
{
  @SuppressWarnings("unused")
  private static final String TAG = ConversationListActivity.class.getSimpleName();

  private NavigationDrawerMessagingFragment mNavigationDrawerMessagingFragment;

  private final DynamicTheme    dynamicTheme    = new DynamicNoActionBarTheme();
  private final DynamicLanguage dynamicLanguage = new DynamicLanguage();

  private ConversationListFragment conversationListFragment;
  private SearchFragment           searchFragment;
  private SearchToolbar            searchToolbar;
  private ImageView                searchAction;
  private ViewGroup                fragmentContainer;

  @Override
  protected void onPreCreate() {
    dynamicTheme.onCreate(this);
    dynamicLanguage.onCreate(this);
  }

  @Override
  protected void onCreate(Bundle icicle, boolean ready) {
    setContentView(R.layout.conversation_list_activity);

    mNavigationDrawerMessagingFragment = (NavigationDrawerMessagingFragment)
        getFragmentManager().findFragmentById(R.id.navigation_drawer);

    // Set up the drawer.
    mNavigationDrawerMessagingFragment.setUp(
        R.id.navigation_drawer,
        (DrawerLayout) findViewById(R.id.container));

    ((DrawerLayout) findViewById(R.id.container)).closeDrawer(GravityCompat.START);

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    searchToolbar            = findViewById(R.id.search_toolbar);
    searchAction             = findViewById(R.id.search_action);
    fragmentContainer        = findViewById(R.id.fragment_container);
    conversationListFragment = initFragment(R.id.fragment_container, new ConversationListFragment(), dynamicLanguage.getCurrentLocale());

    initializeSearchListener();

    RatingManager.showRatingDialogIfNecessary(this);
    RegistrationLockDialog.showReminderIfNecessary(this);

    TooltipCompat.setTooltipText(searchAction, getText(R.string.SearchToolbar_search_for_conversations_contacts_and_messages));
  }

  @Override
  public void onResume() {
    super.onResume();
    dynamicTheme.onResume(this);
    dynamicLanguage.onResume(this);

    initializeProfileIcon();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    MenuInflater inflater = this.getMenuInflater();
    menu.clear();

    inflater.inflate(R.menu.text_secure_normal, menu);

    menu.findItem(R.id.menu_clear_passphrase).setVisible(!TextSecurePreferences.isPasswordDisabled(this));

    super.onPrepareOptionsMenu(menu);
    return true;
  }

  private void initializeSearchListener() {
    searchAction.setOnClickListener(v -> {
      Permissions.with(this)
                 .request(Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS)
                 .ifNecessary()
                 .onAllGranted(() -> searchToolbar.display(searchAction.getX() + (searchAction.getWidth() / 2),
                                                           searchAction.getY() + (searchAction.getHeight() / 2)))
                 .withPermanentDenialDialog(getString(R.string.ConversationListActivity_signal_needs_contacts_permission_in_order_to_search_your_contacts_but_it_has_been_permanently_denied))
                 .execute();
    });

    searchToolbar.setListener(new SearchToolbar.SearchListener() {
      @Override
      public void onSearchTextChange(String text) {
        String trimmed = text.trim();

        if (trimmed.length() > 0) {
          if (searchFragment == null) {
            searchFragment = SearchFragment.newInstance(dynamicLanguage.getCurrentLocale());
            getSupportFragmentManager().beginTransaction()
                                       .add(R.id.fragment_container, searchFragment, null)
                                       .commit();
          }
          searchFragment.updateSearchQuery(trimmed);
        } else if (searchFragment != null) {
          getSupportFragmentManager().beginTransaction()
                                     .remove(searchFragment)
                                     .commit();
          searchFragment = null;
        }
      }

      @Override
      public void onSearchClosed() {
        if (searchFragment != null) {
          getSupportFragmentManager().beginTransaction()
                                     .remove(searchFragment)
                                     .commit();
          searchFragment = null;
        }
      }
    });
  }

  private void initializeProfileIcon() {
    ImageView     icon          = findViewById(R.id.toolbar_icon);
    Address       localAddress  = Address.fromSerialized(TextSecurePreferences.getLocalNumber(this));
    Recipient     recipient     = Recipient.from(this, localAddress, true);
    String        name          = Optional.fromNullable(recipient.getName()).or(Optional.fromNullable(TextSecurePreferences.getProfileName(this))).or("");
    MaterialColor fallbackColor = recipient.getColor();

    if (fallbackColor == ContactColors.UNKNOWN_COLOR && !TextUtils.isEmpty(name)) {
      fallbackColor = ContactColors.generateFor(name);
    }

    Drawable fallback = new GeneratedContactPhoto(name, R.drawable.ic_profile_default).asDrawable(this, fallbackColor.toAvatarColor(this));

    GlideApp.with(this)
            .load(new ProfileContactPhoto(localAddress, String.valueOf(TextSecurePreferences.getProfileAvatarId(this))))
            .error(fallback)
            .circleCrop()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(icon);

    icon.setOnClickListener(v -> handleDisplaySettings());
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    super.onOptionsItemSelected(item);

    switch (item.getItemId()) {
    case R.id.menu_new_group:         createGroup();           return true;
    case R.id.menu_settings:          handleDisplaySettings(); return true;
    case R.id.menu_clear_passphrase:  handleClearPassphrase(); return true;
//    case R.id.menu_mark_all_read:     handleMarkAllRead();     return true;
    case R.id.menu_invite:            handleInvite();          return true;
    case R.id.menu_help:              handleHelp();            return true;
    }

    return false;
  }

  @Override
  public void onCreateConversation(long threadId, Recipient recipient, int distributionType, long lastSeen) {
    openConversation(threadId, recipient, distributionType, lastSeen, -1);
  }

  public void openConversation(long threadId, Recipient recipient, int distributionType, long lastSeen, int startingPosition) {
    searchToolbar.clearFocus();

    Intent intent = new Intent(this, ConversationActivity.class);
    intent.putExtra(ConversationActivity.ADDRESS_EXTRA, recipient.getAddress());
    intent.putExtra(ConversationActivity.THREAD_ID_EXTRA, threadId);
    intent.putExtra(ConversationActivity.DISTRIBUTION_TYPE_EXTRA, distributionType);
    intent.putExtra(ConversationActivity.TIMING_EXTRA, System.currentTimeMillis());
    intent.putExtra(ConversationActivity.LAST_SEEN_EXTRA, lastSeen);
    intent.putExtra(ConversationActivity.STARTING_POSITION_EXTRA, startingPosition);

    startActivity(intent);
    overridePendingTransition(R.anim.slide_from_right, R.anim.fade_scale_out);
  }

  @Override
  public void onSwitchToArchive() {
    Intent intent = new Intent(this, ConversationListArchiveActivity.class);
    startActivity(intent);
  }

  @Override
  public void onBackPressed() {
    if (searchToolbar.isVisible()) searchToolbar.collapse();
    else                           super.onBackPressed();
  }

  private void createGroup() {
    Intent intent = new Intent(this, GroupCreateActivity.class);
    startActivity(intent);
  }

  private void handleDisplaySettings() {
    Intent preferencesIntent = new Intent(this, ApplicationPreferencesActivity.class);
    startActivity(preferencesIntent);
  }

  private void handleClearPassphrase() {
    Intent intent = new Intent(this, KeyCachingService.class);
    intent.setAction(KeyCachingService.CLEAR_KEY_ACTION);
    startService(intent);
  }

  @SuppressLint("StaticFieldLeak")
  private void handleMarkAllRead() {
    new AsyncTask<Void, Void, Void>() {
      @Override
      protected Void doInBackground(Void... params) {
        Context                 context    = ConversationListActivity.this;
        List<MarkedMessageInfo> messageIds = DatabaseFactory.getThreadDatabase(context).setAllThreadsRead();

        MessageNotifier.updateNotification(context);
        MarkReadReceiver.process(context, messageIds);

        return null;
      }
    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }

  private void handleInvite() {
    startActivity(new Intent(this, InviteActivity.class));
  }

  private void handleHelp() {
    try {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://blackcap.zone/support.php")));
    } catch (ActivityNotFoundException e) {
      Toast.makeText(this, R.string.ConversationListActivity_there_is_no_browser_installed_on_your_device, Toast.LENGTH_LONG).show();
    }
  }

  public void openNavigationDrawer(View view) {
    ((DrawerLayout) findViewById(R.id.container)).openDrawer(findViewById(R.id.navigation_drawer));
  }

  @Override
  public void onNavigationDrawerItemSelected(int position) {
    if (position == 0) {
      this.finish();
    } else if (position == 1) {
      Intent intent = new Intent(this, ConversationListActivity.class);
      startActivity(intent);
    } else if (position == 2) {
      String url = "https://blackcap.zone/support.php";

      Intent intent = new Intent(Intent.ACTION_VIEW);
      intent.setData(Uri.parse(url));

      startActivity(intent);

    } else if (position == 3) {
      Intent intent = new Intent(this, BlackcapLicence.class);
      startActivity(intent);
    } else if (position == 4) {
      Intent intent = new Intent(this, BlackcapSettings.class);
      startActivity(intent);
    } else if (position == 5) {
      Intent intent = new Intent(this, SelectLanguageActivity.class);
      startActivity(intent);
    } else if (position == 6) {
      logout();
    }
  }

  public void logout() {
    SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
    SharedPreferences.Editor editor = sharedPreferences.edit();

    editor.putString("username", null);
    editor.putString("password", null);
    editor.putBoolean("auto_login", false);
    editor.putBoolean("save_password", false);
    editor.putBoolean("dont_show_popup", false);
    editor.putBoolean("disconnect_dialog", false);
    editor.apply();

    Intent intent = new Intent(this, BlackcapLogin.class);
    startActivity(intent);

    Intent disconnectVPN = new Intent(this, DisconnectVPN.class);
    startActivity(disconnectVPN);

    finish();
  }
}
