/*
 * Copyright (C) 2015 Open Whisper Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package zone.blackcap.vpn;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.Toast;

import zone.blackcap.vpn.activities.DisconnectVPN;
import zone.blackcap.vpn.activities.SelectLanguageActivity;
import zone.blackcap.vpn.fragments.NavigationDrawerMessagingFragment;
import zone.blackcap.vpn.logging.Log;

import zone.blackcap.vpn.components.ContactFilterToolbar;
import zone.blackcap.vpn.contacts.ContactsCursorLoader.DisplayMode;
import zone.blackcap.vpn.util.DirectoryHelper;
import zone.blackcap.vpn.util.DynamicLanguage;
import zone.blackcap.vpn.util.DynamicNoActionBarTheme;
import zone.blackcap.vpn.util.DynamicTheme;
import zone.blackcap.vpn.util.TextSecurePreferences;
import zone.blackcap.vpn.util.ViewUtil;

import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Base activity container for selecting a list of contacts.
 *
 * @author Moxie Marlinspike
 *
 */
public abstract class ContactSelectionActivity extends PassphraseRequiredActionBarActivity
                                               implements SwipeRefreshLayout.OnRefreshListener,
                                                          ContactSelectionListFragment.OnContactSelectedListener,
                                                          NavigationDrawerMessagingFragment.NavigationDrawerCallbacks
{
  private static final String TAG = ContactSelectionActivity.class.getSimpleName();

  private NavigationDrawerMessagingFragment mNavigationDrawerMessagingFragment;

  private final DynamicTheme    dynamicTheme    = new DynamicNoActionBarTheme();
  private final DynamicLanguage dynamicLanguage = new DynamicLanguage();

  protected ContactSelectionListFragment contactsFragment;

  private ContactFilterToolbar toolbar;

  @Override
  protected void onPreCreate() {
    dynamicTheme.onCreate(this);
    dynamicLanguage.onCreate(this);
  }

  @Override
  protected void onCreate(Bundle icicle, boolean ready) {
    if (!getIntent().hasExtra(ContactSelectionListFragment.DISPLAY_MODE)) {
      int displayMode = TextSecurePreferences.isSmsEnabled(this) ? DisplayMode.FLAG_ALL
                                                                 : DisplayMode.FLAG_PUSH | DisplayMode.FLAG_GROUPS;
      getIntent().putExtra(ContactSelectionListFragment.DISPLAY_MODE, displayMode);
    }

    setContentView(R.layout.contact_selection_activity);

    mNavigationDrawerMessagingFragment = (NavigationDrawerMessagingFragment)
            getFragmentManager().findFragmentById(R.id.navigation_drawer);

    // Set up the drawer.
    mNavigationDrawerMessagingFragment.setUp(
            R.id.navigation_drawer,
            (DrawerLayout) findViewById(R.id.container));

    ((DrawerLayout) findViewById(R.id.container)).closeDrawer(GravityCompat.START);

    initializeToolbar();
    initializeResources();
    initializeSearch();
  }

  @Override
  public void onResume() {
    super.onResume();
    dynamicTheme.onResume(this);
    dynamicLanguage.onResume(this);
  }

  protected ContactFilterToolbar getToolbar() {
    return toolbar;
  }

  private void initializeToolbar() {
    this.toolbar = ViewUtil.findById(this, R.id.toolbar);
    setSupportActionBar(toolbar);

    assert  getSupportActionBar() != null;
    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    getSupportActionBar().setIcon(null);
    getSupportActionBar().setLogo(null);
  }

  private void initializeResources() {
    contactsFragment = (ContactSelectionListFragment) getSupportFragmentManager().findFragmentById(R.id.contact_selection_list_fragment);
    contactsFragment.setOnContactSelectedListener(this);
    contactsFragment.setOnRefreshListener(this);
  }

  private void initializeSearch() {
    toolbar.setOnFilterChangedListener(filter -> contactsFragment.setQueryFilter(filter));
  }

  @Override
  public void onRefresh() {
    new RefreshDirectoryTask(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getApplicationContext());
  }

  @Override
  public void onContactSelected(String number) {}

  @Override
  public void onContactDeselected(String number) {}

  private static class RefreshDirectoryTask extends AsyncTask<Context, Void, Void> {

    private final WeakReference<ContactSelectionActivity> activity;

    private RefreshDirectoryTask(ContactSelectionActivity activity) {
      this.activity = new WeakReference<>(activity);
    }

    @Override
    protected Void doInBackground(Context... params) {

      try {
        DirectoryHelper.refreshDirectory(params[0], true);
      } catch (IOException e) {
        Log.w(TAG, e);
      }

      return null;
    }

    @Override
    protected void onPostExecute(Void result) {
      ContactSelectionActivity activity = this.activity.get();

      if (activity != null && !activity.isFinishing()) {
        activity.toolbar.clear();
        activity.contactsFragment.resetQueryFilter();
      }
    }
  }

  public void openNavigationDrawer(View view) {
    ((DrawerLayout) findViewById(R.id.container)).openDrawer(findViewById(R.id.navigation_drawer));
  }

  @Override
  public void onNavigationDrawerItemSelected(int position) {
    if (position == 0) {
      finish();
    } else if (position == 1) {
      Intent intent = new Intent(this, ConversationListActivity.class);
      startActivity(intent);
    } else if (position == 2) {
      String url = "https://blackcap.zone/support.php";

      Intent intent = new Intent(Intent.ACTION_VIEW);
      intent.setData(Uri.parse(url));

      startActivity(intent);

    } else if (position == 3) {
      Intent intent = new Intent(this, BlackcapLicence.class);
      startActivity(intent);
    } else if (position == 4) {
      Intent intent = new Intent(this, BlackcapSettings.class);
      startActivity(intent);
    } else if (position == 5) {
      Intent intent = new Intent(this, SelectLanguageActivity.class);
      startActivity(intent);
    } else if (position == 6) {
      logout();
    }
  }

  public void logout() {
    SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
    SharedPreferences.Editor editor = sharedPreferences.edit();

    editor.putString("username", null);
    editor.putString("password", null);
    editor.putBoolean("auto_login", false);
    editor.putBoolean("save_password", false);
    editor.putBoolean("dont_show_popup", false);
    editor.putBoolean("disconnect_dialog", false);
    editor.apply();

    Intent intent = new Intent(this, BlackcapLogin.class);
    startActivity(intent);

    Intent disconnectVPN = new Intent(this, DisconnectVPN.class);
    startActivity(disconnectVPN);

    finish();
  }
}
