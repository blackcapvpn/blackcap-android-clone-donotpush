package zone.blackcap.vpn;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.crashlytics.android.Crashlytics;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;

import zone.blackcap.vpn.api.BlackcapApi;

public class BlackcapLogin extends AppCompatActivity implements BlackcapApi.ApiResponseListener {
  private static final String DEBUG_TAG = "Blackcap";

  private static String mUsername;
  private static String mPassword;

  private static ProgressDialog progressDialog_Login;

  public boolean mChecked = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.blackcap_login);

    EditText editText_Username = findViewById(R.id.editText_Username);
    EditText editText_Password = findViewById(R.id.editText_Password);
    Button button_Login = findViewById(R.id.button_Login);
    Button button_ForgotPassword = findViewById(R.id.button_ForgotPassword);

    Typeface roboto = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
    editText_Username.setTypeface(roboto);
    editText_Password.setTypeface(roboto);

    Typeface robotoCondensed = Typeface.createFromAsset(getAssets(), "fonts/RobotoCondensed-Regular.ttf");
    button_Login.setTypeface(robotoCondensed);
    button_ForgotPassword.setTypeface(robotoCondensed);

    SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
    if (sharedPreferences.getBoolean("save_password", false)) {
      editText_Username.setText(sharedPreferences.getString("username", null));
      editText_Password.setText(sharedPreferences.getString("password", null));
    }

    KeyboardVisibilityEvent.setEventListener(
        this,
        isOpen -> {
          if (isOpen) {
            findViewById(R.id.imageView_logo).setVisibility(View.INVISIBLE);
          } else {
            findViewById(R.id.imageView_logo).setVisibility(View.VISIBLE);
          }
        });

    if (sharedPreferences.getBoolean("auto_login", false)) {
      login(null);
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (BlackcapLogin.progressDialog_Login != null && BlackcapLogin.progressDialog_Login.isShowing()) {
      BlackcapLogin.progressDialog_Login.dismiss();
    }
  }

  @Override
  public void onTaskCompleted(BlackcapApi.ApiResponse apiResponse) {
    Crashlytics.setString("apiResponse.requestCode", apiResponse.requestCode.toString());
    Crashlytics.setString("apiResponse.code", apiResponse.code.toString());

    if (apiResponse.requestCode == BlackcapApi.RequestCode.LOGIN) {
      if (BlackcapLogin.progressDialog_Login != null && BlackcapLogin.progressDialog_Login.isShowing()) {
        BlackcapLogin.progressDialog_Login.dismiss();
      }

      if (apiResponse.code == BlackcapApi.ResponseCode.SUCCESS) {
        Crashlytics.setBool("apiResponse.vpn", apiResponse.vpn);
        Crashlytics.setBool("apiResponse.messenger", apiResponse.messenger);
        Crashlytics.setString("BlackcapLogin.mUsername", mUsername);

        SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
        String savedUsername = sharedPreferences.getString("username", null);
        String savedPassword = sharedPreferences.getString("password", null);
        boolean bSavePass = sharedPreferences.getBoolean("save_password", false);
        boolean bDontShowPopup = sharedPreferences.getBoolean("dont_show_popup", false);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("messenger", apiResponse.messenger);
        editor.putBoolean("vpn", apiResponse.vpn);
        editor.putInt("message_count", apiResponse.message_count);
        editor.putString("username", mUsername);
        editor.putString("password", mPassword);
        editor.apply();

        if (!bDontShowPopup) {
          if (bSavePass &&  savedUsername.equalsIgnoreCase(mUsername) && savedPassword.equalsIgnoreCase(mPassword)) {
            openMain();
          } else {
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_save_password, null);
            CheckBox checkBox = dialogView.findViewById(R.id.checkbox);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
              @Override
              public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mChecked = isChecked;
              }
            });

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BlackcapLogin.this, R.style.blackcap_Dialog);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setPositiveButton(android.R.string.yes,
                (dialogInterface, i) -> new AlertDialog.Builder(BlackcapLogin.this, R.style.blackcap_Dialog)
                    .setTitle(getString(R.string.bc_auto_login_title))
                    .setMessage(getString(R.string.bc_auto_login_body))
                    .setPositiveButton(android.R.string.yes,
                        (dialogInterface1, i1) -> {
                          SharedPreferences sharedPreferences1 = getSharedPreferences("BLACKCAP", 0);
                          SharedPreferences.Editor editor1 = sharedPreferences1.edit();
                          editor1.putBoolean("save_password", true);
                          editor1.putBoolean("dont_show_popup", mChecked);
                          editor1.putBoolean("auto_login", true);
                          editor1.apply();

                          openMain();
                        })
                    .setNegativeButton(android.R.string.no,
                        (dialogInterface12, i12) -> {
                          SharedPreferences sharedPreferences1 = getSharedPreferences("BLACKCAP", 0);
                          SharedPreferences.Editor editor1 = sharedPreferences1.edit();
                          editor1.putBoolean("save_password", true);
                          editor1.putBoolean("dont_show_popup", mChecked);
                          editor1.putBoolean("auto_login", false);
                          editor1.apply();

                          openMain();
                        })
                    .show())
                .setNegativeButton(android.R.string.no, (dialogInterface, i) -> {
                  SharedPreferences sharedPreferences12 = getSharedPreferences("BLACKCAP", 0);
                  SharedPreferences.Editor editor1 = sharedPreferences12.edit();
                  editor1.putBoolean("save_password", false);
                  editor1.putBoolean("dont_show_popup", mChecked);
                  editor1.putBoolean("auto_login", false);
                  editor1.apply();

                  openMain();
                });

            AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();          }
        } else {
          openMain();
        }
      } else if (apiResponse.code == BlackcapApi.ResponseCode.INCORRECT_DETAILS) {
        if (!isFinishing()) {
          new AlertDialog.Builder(BlackcapLogin.this, R.style.blackcap_Dialog)
              .setTitle(getString(R.string.bc_error))
              .setMessage(getString(R.string.bc_login_incorrect_details))
              .setPositiveButton(android.R.string.ok, null)
              .show();
        }
      } else if (apiResponse.code == BlackcapApi.ResponseCode.NO_SUBSCRIPTION) {
        if (!isFinishing()) {
          new AlertDialog.Builder(BlackcapLogin.this, R.style.blackcap_Dialog)
              .setTitle(getString(R.string.bc_error))
              .setMessage(getString(R.string.bc_login_no_subscription_msg))
              .setPositiveButton(android.R.string.ok, null)
              .show();
        }
      } else {
        if (!isFinishing()) {
          new AlertDialog.Builder(BlackcapLogin.this, R.style.blackcap_Dialog)
              .setTitle(getString(R.string.bc_error))
              .setMessage(getString(R.string.bc_login_unable_connect))
              .setPositiveButton(android.R.string.ok, null)
              .show();
        }
      }
    }
  }

  public void openMain() {
    Intent intent = new Intent(BlackcapLogin.this, BlackcapMain.class);
    Bundle bundle = new Bundle();
    bundle.putString("username", BlackcapLogin.mUsername);
    bundle.putString("password", BlackcapLogin.mPassword);
    intent.putExtras(bundle);
    startActivity(intent);
    finish();
  }

  public void login(View view) {
    EditText editText_Username = findViewById(R.id.editText_Username);
    EditText editText_Password = findViewById(R.id.editText_Password);

    if (editText_Username != null && editText_Username.getText().toString().isEmpty()) {
      new AlertDialog.Builder(this, R.style.blackcap_Dialog)
          .setTitle(getString(R.string.bc_error))
          .setMessage(getString(R.string.bc_missing_username))
          .setPositiveButton(android.R.string.ok, null)
          .show();
      return;
    } else if (editText_Password != null && editText_Password.getText().toString().isEmpty()) {
      new AlertDialog.Builder(this, R.style.blackcap_Dialog)
          .setTitle(getString(R.string.bc_error))
          .setMessage(getString(R.string.bc_missing_password))
          .setPositiveButton(android.R.string.ok, null)
          .show();
      return;
    }

    if (editText_Username != null && editText_Password != null) {
      ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
      NetworkInfo networkInfo = null;
      if (connMgr != null) {
        networkInfo = connMgr.getActiveNetworkInfo();
      }

      if (networkInfo != null && networkInfo.isConnected()) {
        mUsername = editText_Username.getText().toString();
        mPassword = editText_Password.getText().toString();

        progressDialog_Login = new ProgressDialog(this);
        progressDialog_Login.setTitle(getString(R.string.bc_please_wait));
        progressDialog_Login.setMessage(getString(R.string.bc_logging_in));
        progressDialog_Login.show();

        new BlackcapApi().login(this, mUsername, mPassword);
      } else {
        new AlertDialog.Builder(this, R.style.blackcap_Dialog)
            .setTitle(getString(R.string.bc_error))
            .setMessage(getString(R.string.bc_internet_required))
            .setPositiveButton(android.R.string.ok, null)
            .show();
      }
    }
  }

  public void forgotPassword(View view) {
    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://blackcap.zone/members/pwreset.php"));
    startActivity(intent);
  }
}
