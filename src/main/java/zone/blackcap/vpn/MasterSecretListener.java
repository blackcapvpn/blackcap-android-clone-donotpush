package zone.blackcap.vpn;

public interface MasterSecretListener {
  void onMasterSecretCleared();
}
