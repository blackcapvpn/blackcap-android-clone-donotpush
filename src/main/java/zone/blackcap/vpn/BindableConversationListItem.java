package zone.blackcap.vpn;

import android.support.annotation.NonNull;

import zone.blackcap.vpn.crypto.MasterSecret;
import zone.blackcap.vpn.database.model.ThreadRecord;
import zone.blackcap.vpn.mms.GlideRequests;

import java.util.Locale;
import java.util.Set;

public interface BindableConversationListItem extends Unbindable {

  public void bind(@NonNull ThreadRecord thread,
                   @NonNull GlideRequests glideRequests, @NonNull Locale locale,
                   @NonNull Set<Long> selectedThreads, boolean batchMode);
}
