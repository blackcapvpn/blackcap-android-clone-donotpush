package zone.blackcap.vpn;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;

import zone.blackcap.vpn.activities.DisconnectVPN;
import zone.blackcap.vpn.activities.LogWindow;
import zone.blackcap.vpn.api.BlackcapApi;

public class BlackcapSettings extends AppCompatActivity implements BlackcapApi.ApiResponseListener{
  private static ProgressDialog progressDialog;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.blackcap_settings);

    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(true);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        actionBar.setElevation(0.0f);
      }
    }

    SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);

    CheckBox checkBox_auto_connect = findViewById(R.id.checkBox_autoConnect);
    checkBox_auto_connect.setChecked(sharedPreferences.getBoolean("auto_connect", false));

    CheckBox checkBox_notification = findViewById(R.id.checkBox_notification);
    checkBox_notification.setChecked(sharedPreferences.getBoolean("show_notification", true));
  }

  public void autoConnect(View view) {
    SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
    SharedPreferences.Editor editor = sharedPreferences.edit();

    CheckBox checkBox_auto_connect = (CheckBox) view;
    editor.putBoolean("auto_connect", checkBox_auto_connect.isChecked());
    editor.apply();
  }

  public void showNotification(View view) {
    SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
    SharedPreferences.Editor editor = sharedPreferences.edit();

    CheckBox checkBox_notification = (CheckBox) view;
    editor.putBoolean("show_notification", checkBox_notification.isChecked());
    editor.apply();
  }

  public void openSupport(View view) {
    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://blackcap.zone/support.php"));
    startActivity(intent);
  }

  public void openClientArea(View view) {
    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://blackcap.zone/members/"));
    startActivity(intent);
  }

  public void logOut(View view) {
    SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
    SharedPreferences.Editor editor = sharedPreferences.edit();

    editor.putString("username", null);
    editor.putString("password", null);
    editor.putBoolean("auto_login", false);
    editor.putBoolean("save_password", false);
    editor.putBoolean("dont_show_popup", false);
    editor.putBoolean("disconnect_dialog", false);
    editor.apply();

    Intent intent = new Intent(this, BlackcapLogin.class);
    startActivity(intent);

    Intent disconnectVPN = new Intent(this, DisconnectVPN.class);
    startActivity(disconnectVPN);

    finish();
  }

  public void openLog(View view) {
    Intent intent = new Intent(this, LogWindow.class);
    startActivity(intent);
  }

  public void openAbout(View view) {
    new AlertDialog.Builder(this, R.style.blackcap_Dialog)
        .setTitle(getString(R.string.settings_about))
        .setMessage(getString(R.string.bc_license_thank_you_using) + BuildConfig.VERSION_NAME + getString(R.string.bc_license_rights))
        .setPositiveButton(android.R.string.ok, null)
        .show();
  }

  public void refreshAccount(View view) {
    ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = null;
    if (connMgr != null) {
      networkInfo = connMgr.getActiveNetworkInfo();
    }

    if (networkInfo != null && networkInfo.isConnected()) {

      progressDialog = new ProgressDialog(this);
      progressDialog.setTitle(getString(R.string.bc_please_wait));
      progressDialog.setMessage("Refreshing Account...");
      progressDialog.show();

      SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
      new BlackcapApi().login(this, sharedPreferences.getString("username", ""), sharedPreferences.getString("password", ""));
    } else {
      new AlertDialog.Builder(this, R.style.blackcap_Dialog)
          .setTitle(getString(R.string.bc_error))
          .setMessage(getString(R.string.bc_internet_required))
          .setPositiveButton(android.R.string.ok, null)
          .show();
    }

  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (progressDialog != null && progressDialog.isShowing()) {
      progressDialog.dismiss();
    }
  }

  @Override
  public void onTaskCompleted(BlackcapApi.ApiResponse apiResponse) {
    if (progressDialog != null && progressDialog.isShowing()) {
      progressDialog.dismiss();
    }
    if (apiResponse.requestCode == BlackcapApi.RequestCode.LOGIN) {
      if (apiResponse.code == BlackcapApi.ResponseCode.SUCCESS) {
        SharedPreferences sharedPreferences = getSharedPreferences("BLACKCAP", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean("messenger", apiResponse.messenger);
        editor.putBoolean("vpn", apiResponse.vpn);
        editor.putInt("message_count", apiResponse.message_count);
        editor.apply();
      }
    }
  }
}
