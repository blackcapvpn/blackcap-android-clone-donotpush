package zone.blackcap.vpn.giph.ui;


import android.os.Bundle;
import android.support.v4.content.Loader;

import zone.blackcap.vpn.giph.model.GiphyImage;
import zone.blackcap.vpn.giph.net.GiphyStickerLoader;

import java.util.List;

public class GiphyStickerFragment extends GiphyFragment {
  @Override
  public Loader<List<GiphyImage>> onCreateLoader(int id, Bundle args) {
    return new GiphyStickerLoader(getActivity(), searchString);
  }
}
