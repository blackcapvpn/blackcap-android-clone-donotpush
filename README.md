# Black Cap Android #

Black Cap client for Android. Includes OpenVPN and Signal.

## Building ##
### Cloning ###
~~~
git clone git@bitbucket.org:privacyguardx/blackcap-android.git
cd blackcap-android
git submodule init
~~~

### Compiling ###
Black Cap Android targets two minimul API levels, and so requires 2 apks to be built.

Normal for API 14 and Modern for API 21

